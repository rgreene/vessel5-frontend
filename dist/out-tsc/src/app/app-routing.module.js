var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
var routes = [
    { path: '', redirectTo: 'start', pathMatch: 'full' },
    { path: 'main', loadChildren: './pages/main/main.module#MainPageModule' },
    { path: 'connection', loadChildren: './pages/connection/connection.module#ConnectionPageModule' },
    { path: 'device', loadChildren: './pages/device/device.module#DevicePageModule' },
    { path: 'error', loadChildren: './pages/error/error.module#ErrorPageModule' },
    { path: 'language-location', loadChildren: './pages/language-location/language-location.module#LanguageLocationPageModule' },
    { path: 'product/:id', loadChildren: './pages/product/product.module#ProductPageModule' },
    { path: 'report', loadChildren: './pages/report/report.module#ReportPageModule' },
    { path: 'settings', loadChildren: './pages/settings/settings.module#SettingsPageModule' },
    { path: 'start', loadChildren: './pages/start/start.module#StartPageModule' },
    { path: 'store', loadChildren: './pages/store/store.module#StorePageModule' },
    { path: 'welcome', loadChildren: './pages/welcome/welcome.module#WelcomePageModule' },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        NgModule({
            imports: [RouterModule.forRoot(routes)],
            exports: [RouterModule]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());
export { AppRoutingModule };
//# sourceMappingURL=app-routing.module.js.map
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
import { Platform, ToastController } from '@ionic/angular';
import { ServerAPIService } from './../../providers/server-api.service';
import { Component } from '@angular/core';
import { SerialConnectionService } from '../../providers/serial-connection.service';
import { LoggingService } from '../../providers/logging.service';
import { SecureStorageService } from 'src/app/providers/secure-storage.service';
var DevicePage = /** @class */ (function () {
    function DevicePage(serial, api, logging, secureStorage, toastCtrl, platform) {
        this.serial = serial;
        this.api = api;
        this.logging = logging;
        this.secureStorage = secureStorage;
        this.toastCtrl = toastCtrl;
        this.platform = platform;
        this.logMessage = '';
        this.version = '1.0.0';
        this.deviceIP = '127.0.0.1';
        this.deviceId = 'xxxxx-xxxxxxx-xxxxxxx-xxxxx';
        this.cloudVersion = '1.0.1';
    }
    DevicePage.prototype.ngOnInit = function () {
    };
    DevicePage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.logging.info('ionViewDidLoad DevicePage');
        // get the status
        this.serial.$.subscribe(function (data) {
            _this.portStatus = data;
        });
        // get the last command
        this.serial.prevCommand$.subscribe(function (data) {
            _this.portData = data;
        });
        this.logging.logEntry$.subscribe(function (data) {
            _this.logMessage = JSON.stringify(data);
        });
    };
    // Start the serial listener
    DevicePage.prototype.startSerial = function (driver) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        // start serial connection
                        console.log('startSerial..');
                        return [4 /*yield*/, this.serial.request()
                                .then(function (result) { return __awaiter(_this, void 0, void 0, function () {
                                var _this = this;
                                return __generator(this, function (_a) {
                                    switch (_a.label) {
                                        case 0:
                                            if (!result) {
                                                return [2 /*return*/];
                                            }
                                            return [4 /*yield*/, this.serial.start(57600)
                                                    .then(function () { return _this.logging.info('serial started'); })
                                                    .catch(function () { return _this.logging.error('error starting serial'); })];
                                        case 1:
                                            _a.sent();
                                            return [2 /*return*/];
                                    }
                                });
                            }); })
                                .catch(function () { return _this.logging.error('error requesting serial'); })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    ///  Do a factory reset
    DevicePage.prototype.factoryReset = function () {
        return __awaiter(this, void 0, void 0, function () {
            var toast;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        // clear out the storage
                        this.secureStorage.clear();
                        return [4 /*yield*/, this.toastCtrl.create({
                                message: 'device reset.',
                                duration: 2000
                            })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    // Start the serial listener
    DevicePage.prototype.sendMessage = function () {
        return __awaiter(this, void 0, void 0, function () {
            var message;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        // start serial connection
                        console.log('sendMessage..');
                        message = {
                            ProtocolVersion: 5,
                            SenderModuleType: 5,
                            SenderModuleID: 6,
                            TargetModuleType: 6,
                            TargetModuleID: 6,
                            Instruction: 7,
                            DataSize: 8,
                            Data0: 9,
                            Data1: 9,
                            Data2: 9,
                            Data3: 9,
                            Data4: 9,
                            Data5: 9,
                            Data6: 9,
                            Data7: 9
                        };
                        return [4 /*yield*/, this.api.sendDeviceMessage(message)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    DevicePage = __decorate([
        Component({
            selector: 'app-device',
            templateUrl: './device.page.html',
            styleUrls: ['./device.page.scss'],
        }),
        __metadata("design:paramtypes", [SerialConnectionService,
            ServerAPIService,
            LoggingService,
            SecureStorageService,
            ToastController,
            Platform])
    ], DevicePage);
    return DevicePage;
}());
export { DevicePage };
//# sourceMappingURL=device.page.js.map
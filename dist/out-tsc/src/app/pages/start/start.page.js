var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
import { Component } from '@angular/core';
import { NavController, Platform } from '@ionic/angular';
import { SerialConnectionService } from '../../providers/serial-connection.service';
import { LoggingService } from './../../providers/logging.service';
import { SecureStorageService } from './../../providers/secure-storage.service';
import { environment } from 'src/environments/environment.prod';
var StartPage = /** @class */ (function () {
    function StartPage(serial, secureStroageService, logging, navCtrl, platform) {
        var _this = this;
        this.serial = serial;
        this.secureStroageService = secureStroageService;
        this.logging = logging;
        this.navCtrl = navCtrl;
        this.platform = platform;
        this.secureStroageService.UserPreference$.subscribe(function (data) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.logging.info(JSON.stringify(data));
                        // if (data != null) {
                        //   this.navCtrl.navigateRoot('/main')
                        //   .then((result) =>  this.logging.info(`result ${result}`) )
                        //   .catch((err) => { this.logging.error(`error on navigation ${err}`); });
                        //   return;
                        // }
                        // set default preferences
                        this.secureStroageService._userPreference = {
                            setup: true,
                            deviceName: 'test',
                            deviceUUID: 'test',
                            deviceLocation: 'test',
                        };
                        // persist the results
                        return [4 /*yield*/, this.secureStroageService.save()];
                    case 1:
                        // persist the results
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        }); });
    }
    StartPage.prototype.ngOnInit = function () {
        var _this = this;
        this.logging.info('Startup loaded');
        // check to see if we have existing data stored
        this.platform.ready().then(function () {
            if (_this.platform.is('android') === true) {
                _this.startSerial();
                _this.secureStroageService.refresh();
            }
            if (_this.platform.is('desktop') === true) {
                _this.navCtrl.navigateRoot('/main');
            }
        });
    };
    StartPage.prototype.startSerial = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: 
                    // start serial connection
                    return [4 /*yield*/, this.serial.request(environment.SERIAL_DRIVER)
                            .then(function (result) { return __awaiter(_this, void 0, void 0, function () {
                            var _this = this;
                            return __generator(this, function (_a) {
                                switch (_a.label) {
                                    case 0:
                                        if (!result) {
                                            return [2 /*return*/];
                                        }
                                        return [4 /*yield*/, this.serial.start(environment.SERIAL_SPEED)
                                                .then(function () { return _this.logging.info('serial started'); })
                                                .catch(function () { return _this.logging.error('error starting serial'); })];
                                    case 1:
                                        _a.sent();
                                        return [2 /*return*/];
                                }
                            });
                        }); })
                            .catch(function () { return _this.logging.error('error requesting serial'); })];
                    case 1:
                        // start serial connection
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    StartPage = __decorate([
        Component({
            selector: 'app-start',
            templateUrl: './start.page.html',
            styleUrls: ['./start.page.scss']
        }),
        __metadata("design:paramtypes", [SerialConnectionService,
            SecureStorageService,
            LoggingService,
            NavController,
            Platform])
    ], StartPage);
    return StartPage;
}());
export { StartPage };
//# sourceMappingURL=start.page.js.map
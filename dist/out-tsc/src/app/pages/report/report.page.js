var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { ChartComponent } from '../../components/chart/chart.component';
var ReportPage = /** @class */ (function () {
    function ReportPage() {
    }
    ReportPage.prototype.ngOnInit = function () {
    };
    ReportPage.prototype.ngAfterContentInit = function () {
        console.log('cccc');
        this._graphOption = [
            { id: 1, title: 'Running Costs', selected: true },
            { id: 2, title: 'Income', selected: false },
            { id: 3, title: 'Beverage', selected: false }
        ];
        this._graphPeriod = [
            { id: 1, title: 'Weekly', selected: true,
                graphData: {
                    labels: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
                    datasets: [{
                            label: '# of Votes',
                            data: [12, 19, 3, 5, 2, 3, 7, 5],
                            borderWidth: 1
                        }]
                }
            },
            { id: 2, title: 'Monthly', selected: false,
                graphData: {
                    labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'],
                    datasets: [{
                            label: '# ',
                            data: [12, 19, 3, 5, 2, 3, 7, 12, 19, 3, 5, 2],
                            borderWidth: 1
                        }]
                }
            },
            { id: 3, title: 'Quarterly', selected: false,
                graphData: {
                    labels: ['Q1', 'Q2', 'Q3', 'Q4'],
                    datasets: [{
                            label: '#',
                            data: [40, 20, 30, 70],
                            borderWidth: 1
                        }]
                }
            },
            { id: 4, title: 'Custom', selected: false,
                graphData: {
                    labels: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
                    datasets: [{
                            label: '#',
                            data: [12, 19, 3, 5, 2, 3, 7],
                            borderWidth: 1
                        }]
                }
            }
        ];
        this.updateGraph();
    };
    ReportPage.prototype.setGraphOption = function (event) {
        // clear selection
        this._graphOption.forEach(function (x) { return x.selected = false; });
        // set the selected
        var elementId = event.target.id;
        var option = this._graphOption.find(function (x) { return x.id === elementId; });
        option.selected = true;
        var index = this._graphOption.indexOf(option);
        this._graphOption[index] = option;
        this.updateGraph();
    };
    ReportPage.prototype.setGraphPeriod = function (event) {
        // clear selection
        this._graphPeriod.forEach(function (x) { return x.selected = false; });
        // set the selected
        var elementId = event.target.id;
        var option = this._graphPeriod.find(function (x) { return x.id === elementId; });
        option.selected = true;
        var index = this._graphPeriod.indexOf(option);
        this._graphPeriod[index] = option;
        this.updateGraph();
    };
    ReportPage.prototype.updateGraph = function () {
        var option = this._graphPeriod.find(function (x) { return x.selected; });
        console.log(option);
        this._barData = option.graphData;
        this.chart1.update();
    };
    __decorate([
        ViewChild('chart1'),
        __metadata("design:type", ChartComponent)
    ], ReportPage.prototype, "chart1", void 0);
    ReportPage = __decorate([
        Component({
            selector: 'app-report',
            templateUrl: './report.page.html',
            styleUrls: ['./report.page.scss'],
        }),
        __metadata("design:paramtypes", [])
    ], ReportPage);
    return ReportPage;
}());
export { ReportPage };
//# sourceMappingURL=report.page.js.map
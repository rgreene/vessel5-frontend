var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { CommonModule } from '@angular/common';
import { BluetoothSetupComponent } from './bluetooth-setup/bluetooth-setup.component';
import { NetworkSetupComponent } from './network-setup/network-setup.component';
import { LanguageComponent } from './language/language.component';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { UnderConstructionComponent } from './under-construction/under-construction.component';
import { LocationComponent } from './location/location.component';
import { GasStatusComponent } from './gas-status/gas-status.component';
import { ChartComponent } from './chart/chart.component';
import { KegStatusComponent } from '../components/keg-status/keg-status.component';
import { SerialConnectionService } from '../providers/serial-connection.service';
import { LoggingService } from './../providers/logging.service';
import { SecureStorageService } from './../providers/secure-storage.service';
var SharedModule = /** @class */ (function () {
    function SharedModule() {
    }
    SharedModule = __decorate([
        NgModule({
            imports: [
                CommonModule,
            ],
            providers: [
                SecureStorageService, SerialConnectionService, LoggingService
            ],
            declarations: [
                UnderConstructionComponent,
                KegStatusComponent,
                GasStatusComponent,
                ChartComponent,
                LanguageComponent,
                LocationComponent,
                NetworkSetupComponent,
                BluetoothSetupComponent
            ],
            exports: [
                UnderConstructionComponent,
                KegStatusComponent,
                GasStatusComponent,
                ChartComponent,
                LocationComponent,
                LanguageComponent,
                NetworkSetupComponent,
                BluetoothSetupComponent
            ],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        })
    ], SharedModule);
    return SharedModule;
}());
export { SharedModule };
//# sourceMappingURL=shared.module.js.map
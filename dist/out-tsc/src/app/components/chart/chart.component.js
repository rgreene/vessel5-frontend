var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Chart } from 'chart.js';
import { Component, ViewChild, Input } from '@angular/core';
var ChartComponent = /** @class */ (function () {
    function ChartComponent() {
    }
    Object.defineProperty(ChartComponent.prototype, "graphType", {
        set: function (graphType) {
            this._graphType = graphType || 'bar';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ChartComponent.prototype, "graphData", {
        set: function (graphData) {
            console.log(graphData);
            this._graphData = graphData || null;
            this.createGraph();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ChartComponent.prototype, "graphOptions", {
        set: function (graphOptions) {
            this._graphOptions = graphOptions || null;
        },
        enumerable: true,
        configurable: true
    });
    ChartComponent.prototype.ngOnInit = function () { };
    ChartComponent.prototype.createGraph = function () {
        this._canvas = new Chart(this.graphCanvas.nativeElement, {
            type: this._graphType,
            data: this._graphData,
            options: {
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    yAxes: [
                        {
                            ticks: {
                                beginAtZero: true
                            }
                        }
                    ]
                }
            }
        });
    };
    ChartComponent.prototype.update = function () {
        //  console.log('update graph');
        //  this._canvas.update();
    };
    __decorate([
        Input(),
        __metadata("design:type", String),
        __metadata("design:paramtypes", [String])
    ], ChartComponent.prototype, "graphType", null);
    __decorate([
        Input(),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], ChartComponent.prototype, "graphData", null);
    __decorate([
        Input(),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], ChartComponent.prototype, "graphOptions", null);
    __decorate([
        ViewChild('graphCanvas'),
        __metadata("design:type", Object)
    ], ChartComponent.prototype, "graphCanvas", void 0);
    ChartComponent = __decorate([
        Component({
            selector: 'app-chart',
            templateUrl: './chart.component.html',
            styleUrls: ['./chart.component.scss']
        }),
        __metadata("design:paramtypes", [])
    ], ChartComponent);
    return ChartComponent;
}());
export { ChartComponent };
//# sourceMappingURL=chart.component.js.map
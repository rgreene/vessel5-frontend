var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input, Output, EventEmitter } from '@angular/core';
var KegStatusComponent = /** @class */ (function () {
    function KegStatusComponent() {
        this._kegClassName = 'active';
        this._id = 0;
        this._temp = 0;
        this._pint = 0;
        this._glass = 0;
        this._logo = 'icon-glass-beer.svg';
        this.selected = new EventEmitter();
    }
    Object.defineProperty(KegStatusComponent.prototype, "keg", {
        get: function () {
            return this._keg;
        },
        set: function (keg) {
            this._keg = keg;
            this._id = keg.id;
            this._temp = keg.temp;
            this._pint = keg.pint;
            this._glass = keg.glass;
            this._active = keg.active;
            this._beerName = keg.beerName;
            this._description = keg.description;
            this._logo = "url(./assets/imgs/" + (keg.logo || 'logo.png') + ")";
            this._kegClassName = keg.active ? 'active' : 'inactive';
        },
        enumerable: true,
        configurable: true
    });
    KegStatusComponent.prototype.ngOnInit = function () { };
    KegStatusComponent.prototype.select = function () {
        this.selected.emit(this._keg);
    };
    __decorate([
        Input(),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], KegStatusComponent.prototype, "keg", null);
    __decorate([
        Output(),
        __metadata("design:type", EventEmitter)
    ], KegStatusComponent.prototype, "selected", void 0);
    KegStatusComponent = __decorate([
        Component({
            selector: 'app-keg-status',
            templateUrl: './keg-status.component.html',
            styleUrls: ['./keg-status.component.scss']
        }),
        __metadata("design:paramtypes", [])
    ], KegStatusComponent);
    return KegStatusComponent;
}());
export { KegStatusComponent };
//# sourceMappingURL=keg-status.component.js.map
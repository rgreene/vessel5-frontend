var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, Input } from '@angular/core';
var GasStatusComponent = /** @class */ (function () {
    function GasStatusComponent() {
        this._gas = {
            colour: '',
            status: '',
            percentFull: 50,
            percent: .5,
            name: 'unknown'
        };
    }
    Object.defineProperty(GasStatusComponent.prototype, "gas", {
        set: function (gas) {
            this._gas = gas;
        },
        enumerable: true,
        configurable: true
    });
    GasStatusComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        Input(),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], GasStatusComponent.prototype, "gas", null);
    GasStatusComponent = __decorate([
        Component({
            selector: 'app-gas-status',
            templateUrl: './gas-status.component.html',
            styleUrls: ['./gas-status.component.scss']
        }),
        __metadata("design:paramtypes", [])
    ], GasStatusComponent);
    return GasStatusComponent;
}());
export { GasStatusComponent };
//# sourceMappingURL=gas-status.component.js.map
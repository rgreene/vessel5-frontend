import { async, TestBed } from '@angular/core/testing';
import { BluetoothSetupComponent } from './bluetooth-setup.component';
describe('BluetoothSetupComponent', function () {
    var component;
    var fixture;
    beforeEach(async(function () {
        TestBed.configureTestingModule({
            declarations: [BluetoothSetupComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = TestBed.createComponent(BluetoothSetupComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=bluetooth-setup.component.spec.js.map
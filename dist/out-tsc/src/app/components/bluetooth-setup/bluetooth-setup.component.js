var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
var BluetoothSetupComponent = /** @class */ (function () {
    function BluetoothSetupComponent() {
        this.network = [];
    }
    BluetoothSetupComponent.prototype.ngOnInit = function () {
        this.network.push({ name: 'Vessel 5', icon: 'bluetooth' });
    };
    BluetoothSetupComponent = __decorate([
        Component({
            selector: 'app-bluetooth-setup',
            templateUrl: './bluetooth-setup.component.html',
            styleUrls: ['./bluetooth-setup.component.scss']
        }),
        __metadata("design:paramtypes", [])
    ], BluetoothSetupComponent);
    return BluetoothSetupComponent;
}());
export { BluetoothSetupComponent };
//# sourceMappingURL=bluetooth-setup.component.js.map
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
import { Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Device } from '@ionic-native/device/ngx';
import { LoggingService } from './logging.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Platform } from '@ionic/angular';
var ServerAPIService = /** @class */ (function () {
    function ServerAPIService(http, device, geolocation, logging, platform) {
        var _this = this;
        this.http = http;
        this.device = device;
        this.geolocation = geolocation;
        this.logging = logging;
        this.platform = platform;
        // standard Header for API call
        this.headers = new Headers({
            'Content-Type': 'application/json',
            'x-api-key': environment.API_KEY
        });
        // exit if we are on the browser
        this.platform.ready().then(function (env) {
            _this.logging.info(env);
            if (env === 'cordova') {
                // get current gps just once as we're not very pushed on accurate details
                _this.geolocation
                    .getCurrentPosition({
                    maximumAge: 3000,
                    timeout: 5000,
                    enableHighAccuracy: false
                })
                    .then(function (position) {
                    _this.longitude = position.coords.longitude;
                    _this.latitude = position.coords.latitude;
                })
                    .catch(function (err) {
                    console.error(`Error getting location. ${err.code} / ${err.message}`);
                });
            }
        });
    }
    // Send a message to the cloud
    ServerAPIService.prototype.sendDeviceMessage = function (message) {
        return __awaiter(this, void 0, void 0, function () {
            var data;
            var _this = this;
            return __generator(this, function (_a) {
                data = this.GenerateDeviceMessage(message);
                // fire and forget
                console.log(JSON.stringify(data));
                this.http
                    .post(environment.API_DEVICE_ENDPOINT, data, { headers: this.headers })
                    .subscribe(function () { return _this.logging.info('sent to server'); }, function (err) { return _this.logging.error('data not sent to server : ' + err); });
                return [2 /*return*/];
            });
        });
    };
    // Format the message for sending to the cloud
    ServerAPIService.prototype.GenerateDeviceMessage = function (message) {
        return {
            UUID: this.device.uuid || '000',
            DeviceID: environment.DEVICE_ID || 0,
            // -- Values
            ProtocolVersion: message.ProtocolVersion,
            SenderModuleType: message.SenderModuleType,
            SenderModuleID: message.SenderModuleID,
            TargetModuleType: message.TargetModuleType,
            TargetModuleID: message.TargetModuleID,
            Instruction: message.Instruction,
            DataSize: message.DataSize,
            Data0: message.Data0,
            Data1: message.Data1,
            Data2: message.Data2,
            Data3: message.Data3,
            Data4: message.Data4,
            Data5: message.Data5,
            Data6: message.Data6,
            Data7: message.Data7,
            // -- Location
            Lng: this.longitude || 0,
            Lat: this.latitude || 0,
            // -- vector sequence
            sequence: 1,
            Message: 'Device Event'
        };
    };
    ServerAPIService = __decorate([
        Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [Http,
            Device,
            Geolocation,
            LoggingService,
            Platform])
    ], ServerAPIService);
    return ServerAPIService;
}());
export { ServerAPIService };
//# sourceMappingURL=server-api.service.js.map
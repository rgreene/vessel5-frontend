var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Serial } from '@ionic-native/serial/ngx';
import { Platform } from '@ionic/angular';
import { ServerAPIService } from './server-api.service';
import { LoggingService } from './logging.service';
import { Subject } from 'rxjs';
import { environment } from '../../environments/environment';
var SerialConnectionService = /** @class */ (function () {
    function SerialConnectionService(http, serial, api, platform, logging) {
        var _this = this;
        this.http = http;
        this.serial = serial;
        this.api = api;
        this.platform = platform;
        this.logging = logging;
        // Observable string streams
        this.serialStatus = new Subject();
        this.prevCommand = new Subject();
        this.prevCommand$ = this.prevCommand.asObservable();
        this.serialStatus$ = this.prevCommand.asObservable();
        // command in progress
        this.currentCommand = '';
        // exit if we are on the browser
        this.platform.ready().then(function (env) {
            _this.logging.info(env);
            if (env === 'cordova') {
                logging.info('logging registered');
                // register command creator for any text arriving
                _this.serial.registerReadCallback().subscribe(function (data) {
                    _this.buildCommand(_this.ab2str(data));
                });
            }
        });
    }
    SerialConnectionService.prototype.request = function (driver) {
        if (driver === void 0) { driver = null; }
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.requestPermission(driver || environment.SERIAL_DRIVER)
                            .then(function (permission) {
                            _this.deviceConnected = permission;
                            return _this.deviceConnected;
                        })
                            .catch(function (err) {
                            _this.logging.warning('Error getting permisson:' + err);
                            _this.deviceConnected = false;
                            return _this.deviceConnected;
                        })];
                    case 1: 
                    // request permission
                    return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    SerialConnectionService.prototype.start = function (baud) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: 
                    // Open the connection
                    return [4 /*yield*/, this.open(baud)
                            .then(function (res) {
                            _this.devicePortOpen = true;
                            return _this.devicePortOpen;
                        })
                            .catch(function (err) {
                            _this.devicePortOpen = false;
                            _this.logging.error('Error opening port');
                        })];
                    case 1:
                        // Open the connection
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    SerialConnectionService.prototype.stop = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: 
                    // request permission
                    return [4 /*yield*/, this.closeSerialDataConnection()
                            .then(function (res) {
                            _this.devicePortOpen = false;
                        })
                            .catch(function (err) {
                            _this.logging.error('Error closing port');
                        })];
                    case 1:
                        // request permission
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    SerialConnectionService.prototype.open = function (baud) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.openSerialDataConnection(baud)
                            .then(function (data) {
                            _this.logging.info('opened..');
                            return true;
                        })
                            .catch(function (err) {
                            _this.logging.error(err);
                            return false;
                        })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    // Send a command via the provider to the serial connection
    // delimitator is atomatically added to the command
    // Command are send to the device using the format
    // SYS TX: 0 2 1 1 1 0 8 128 0 0 0 0 0 0 0 \n
    //
    SerialConnectionService.prototype.writeData = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            var command;
            var _this = this;
            return __generator(this, function (_a) {
                command = "SYS TX: " + data.ProtocolVersion + " " + data.SenderModuleType + " " + data.SenderModuleID + " " + data.TargetModuleType + " " + data.TargetModuleID + " " + data.Instruction + " " + data.DataSize + " " + data.Data0 + " " + data.Data1 + " " + data.Data2 + " " + data.Data3 + " " + data.Data4 + " " + data.Data5 + " " + data.Data6 + " " + data.Data7;
                this.logging.info('writting data to serial:' + command);
                this.serial.write(command + environment.SERIAL_DELIMITOR)
                    .then(function (res) { return _this.logging.info('wrote data:' + command); })
                    .catch(function (error) { return _this.logging.error(error); });
                return [2 /*return*/];
            });
        });
    };
    SerialConnectionService.prototype.stopData = function () {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        this.logging.info('Stopping data');
                        return [4 /*yield*/, this.closeSerialDataConnection()];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    //  Request permission
    SerialConnectionService.prototype.requestPermission = function (driver) {
        return __awaiter(this, void 0, void 0, function () {
            var opt;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        opt = { vid: '04D8', pid: '00DF', driver: driver };
                        return [4 /*yield*/, this.serial.requestPermission(opt)
                                .then(function () {
                                _this.logging.info('permission given!!!');
                                return true;
                            })
                                .catch(function () {
                                _this.logging.warning('permission not given.');
                                return false;
                            })];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    // Open the connection
    SerialConnectionService.prototype.openSerialDataConnection = function (baud) {
        return __awaiter(this, void 0, void 0, function () {
            var options, err_1;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        options = { baudRate: baud,
                            dataBits: 8,
                            stopBits: 1,
                            parity: 0,
                            dtr: false,
                            rts: false,
                            sleepOnPause: false };
                        return [4 /*yield*/, this.serial.open(options)
                                .then(function () {
                                _this.logging.info('opened');
                                return (true);
                            })
                                .catch(function (error) {
                                _this.logging.error('error ' + error);
                                return (false);
                            })];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        err_1 = _a.sent();
                        this.logging.error('error:' + err_1);
                        return [2 /*return*/, (false)];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    //  Close the connection
    SerialConnectionService.prototype.closeSerialDataConnection = function () {
        return __awaiter(this, void 0, void 0, function () {
            var err_2;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.serial.close()
                                .then(function () {
                                _this.logging.info('closed serial connection');
                                return (true);
                            })
                                .catch(function (error) {
                                _this.logging.error('error closing connection' + error);
                                return (false);
                            })];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        err_2 = _a.sent();
                        this.logging.warning('unexpected error closing connection:' + err_2);
                        return [2 /*return*/, (false)];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    //  Build a command from the fragments provided by the serial connection
    // check if the delimitator has been passed if so execute the command
    // Command are send to the device using the format
    // TX DATA: 0 2 1 1 1 0 8 128 0 0 0 0 0 0 0 \n
    // Commands are returned from device using format
    // RX DATA: 0 1 0 2 1 1 8 128 0 0 D1 D2 D3 D4 0 \n"
    //
    SerialConnectionService.prototype.buildCommand = function (text) {
        var terminator = text.indexOf(environment.SERIAL_DELIMITOR);
        // have we reached the command terminator?
        if (terminator === -1) {
            this.currentCommand = this.currentCommand.concat(text);
            return;
        }
        this.currentCommand = this.currentCommand.concat(text.substring(0, terminator));
        /* convert the string into a valid object
        // example
        // RX DATA: 0 1 1 2 1 1 8 128 1 1 27 0 0 0 0
        // only send the SYS RX messages for the moment
        */
        if (this.currentCommand.startsWith('RX DATA:')) {
            this.api.sendDeviceMessage(this.toSerialCommand(this.currentCommand));
            this.prevCommand.next(this.toSerialCommand(this.currentCommand));
        }
        // publish the command to any subscribers
        this.currentCommand = text.substring(terminator + 1);
    };
    // take a standard string and parse it for the various details
    SerialConnectionService.prototype.toSerialCommand = function (command) {
        // remove the header
        command = command.replace('RX DATA: ', '');
        var params = command.split(' ');
        this.logging.info("params " + params.length);
        var result = {
            ProtocolVersion: Number(params[0]),
            SenderModuleType: Number(params[1]),
            SenderModuleID: Number(params[2]),
            TargetModuleType: Number(params[3]),
            TargetModuleID: Number(params[4]),
            Instruction: Number(params[5]),
            DataSize: Number(params[6]),
            Data0: Number(params[7]),
            Data1: Number(params[8]),
            Data2: Number(params[9]),
            Data3: Number(params[10]),
            Data4: Number(params[11]),
            Data5: Number(params[12]),
            Data6: Number(params[13]),
            Data7: Number(params[14])
        };
        return result;
    };
    // simple tranform process
    SerialConnectionService.prototype.ab2str = function (buf) {
        return String.fromCharCode.apply(null, new Uint8Array(buf));
    };
    SerialConnectionService = __decorate([
        Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [Http,
            Serial,
            ServerAPIService,
            Platform,
            LoggingService])
    ], SerialConnectionService);
    return SerialConnectionService;
}());
export { SerialConnectionService };
//# sourceMappingURL=serial-connection.service.js.map
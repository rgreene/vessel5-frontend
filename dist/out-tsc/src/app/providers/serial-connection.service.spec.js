import { TestBed } from '@angular/core/testing';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { Serial } from '@ionic-native/serial/ngx';
import { Device } from '@ionic-native/device/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { SerialConnectionService } from './serial-connection.service';
describe('SerialConnectionService', function () {
    beforeEach(function () { return TestBed.configureTestingModule({
        imports: [BrowserModule, HttpModule],
        providers: [Serial, Device, Geolocation],
    }); });
    it('should be created', function () {
        var service = TestBed.get(SerialConnectionService);
        expect(service).toBeTruthy();
    });
});
//# sourceMappingURL=serial-connection.service.spec.js.map
var SensorFactory;
(function (SensorFactory) {
    // types of sensors
    var SensorType;
    (function (SensorType) {
        SensorType[SensorType["Tempature"] = 0] = "Tempature";
        SensorType[SensorType["Pressure"] = 1] = "Pressure";
    })(SensorType = SensorFactory.SensorType || (SensorFactory.SensorType = {}));
    var Tempature = /** @class */ (function () {
        function Tempature() {
            var _this = this;
            this.update = function (param) {
                _this.value = param || 0;
            };
        }
        return Tempature;
    }());
    SensorFactory.Tempature = Tempature;
    var Pressure = /** @class */ (function () {
        function Pressure() {
            var _this = this;
            this.update = function (param) {
                _this.value = param || 0;
            };
        }
        return Pressure;
    }());
    SensorFactory.Pressure = Pressure;
    // Create Sensor
    function connectSensor(type) {
        switch (type) {
            case SensorType.Tempature:
                return new Tempature();
            case SensorType.Pressure:
                return new Pressure();
        }
        return null;
    }
    SensorFactory.connectSensor = connectSensor;
})(SensorFactory || (SensorFactory = {}));
//# sourceMappingURL=sensorFactory.js.map
// ------------------------------------------------------------
//   Set the default data for on startup
// ------------------------------------------------------------
var DefaultObjects = /** @class */ (function () {
    function DefaultObjects() {
    }
    // -- default user data
    DefaultObjects.prototype.User = function () {
        return {
            setup: true,
            deviceName: 'test',
            deviceUUID: 'test',
            deviceLocation: 'test',
        };
    };
    // -- default kegs on device
    DefaultObjects.prototype.Kegs = function () {
        var defaultKegs = [];
        defaultKegs.push({
            id: 1,
            name: 'Keg1',
            active: true,
            glass: 0,
            pint: 0,
            percentFull: 0,
            stength: 0,
            beerName: 'Single Fin',
            logo: 'beers/singlefin-logo.png',
            description: 'Brewed with top fermenting yeast at cellar temperature, ales are fuller-bodied, with nuances of fruit or spice and a pleasantly hoppy finish. Generally robust and complex with a variety of fruit and malt aromas.'
        });
        defaultKegs.push({
            id: 2,
            name: 'Keg2',
            active: false,
            glass: 0,
            pint: 0,
            percentFull: 0,
            stength: 0,
        });
        defaultKegs.push({
            id: 3,
            name: 'Keg3',
            active: false,
            glass: 0,
            pint: 0,
            percentFull: 0,
            stength: 0,
        });
        defaultKegs.push({
            id: 4,
            name: 'Keg4',
            active: false,
            glass: 0,
            pint: 0,
            percentFull: 0,
            stength: 0,
        });
        return defaultKegs;
    };
    // setup default gas levels on device
    DefaultObjects.prototype.Gas = function () {
        var defaultGas = [];
        defaultGas.push({
            name: 'Nitrogen',
            status: 'Connected',
            percent: 0,
            percentFull: 0.0,
            colour: '#FFFFFF'
        });
        defaultGas.push({
            name: 'CO2',
            status: 'Connected',
            percent: 0,
            percentFull: 0.0,
            colour: '#FFFFFF'
        });
        return defaultGas;
    };
    // ------------------------------------------------------------
    //   Set the test data for use in testing and on startup
    // ------------------------------------------------------------
    DefaultObjects.prototype.TestOneKeg = function () {
        var testKegs = [];
        testKegs.push({
            id: 1,
            name: 'T1',
            active: true,
            glass: 0,
            pint: 0,
            percentFull: 0,
            stength: 0,
            beerName: 'test beer',
            description: 'test one Keg.'
        });
        return testKegs;
    };
    DefaultObjects.prototype.TestGas = function () {
        var testGas = [];
        testGas.push({
            name: 'Nitrogen',
            status: 'Connected',
            percent: 50,
            percentFull: 0.5,
            colour: '#FFFFFF'
        });
        testGas.push({
            name: 'CO2',
            status: 'Connected',
            percent: 5,
            percentFull: 0.5,
            colour: '#FFFFFF'
        });
        return testGas;
    };
    return DefaultObjects;
}());
export { DefaultObjects };
//# sourceMappingURL=defaultDataObjects.js.map
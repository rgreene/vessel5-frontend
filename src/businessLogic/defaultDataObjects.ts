import { V5Types } from 'src/typings/cordova-typings';

// ------------------------------------------------------------
//   Set the default data for on startup
// ------------------------------------------------------------
export class DefaultObjects {

  public Default50LiterKeg(): V5Types.IKeg { return <V5Types.IKeg>{
  id: 0,
  name: '',
  active: true,
  glass: 0,
  pint: 0,
  temp: 0,
  totalVolume: 50000,  // 50 liters default keg
  servedVolume: 0,  // full
  percentFull: 0,
  stength: 0,
  beerName: '',
  logo: null,
  description: null
  };
}

  // -- default user data
  public User(): V5Types.IUserPreference {
      return <V5Types.IUserPreference>{
      setup : true,
      deviceName : 'test',
      deviceUUID : 'test',
      deviceLocation : 'test',
    };
  }

  // -- default kegs on device
  public Kegs(): V5Types.IKeg[] {
    const defaultKegs: V5Types.IKeg[] = [];
      defaultKegs.push(<V5Types.IKeg>{
      id: 0,
      name: 'Keg1',
      active: true,
      glass: 0,
      pint: 0,
      totalVolume: 50000,  // 50 liters default keg
      servedVolume: 25000,  // half full
      percentFull: 0,
      stength: 0,
      beerName: 'Single Fin',
      logo: 'beers/singlefin-logo.png',
      description:
        'Brewed with top fermenting yeast at cellar temperature, ales are fuller-bodied, with nuances of fruit or spice and a pleasantly hoppy finish. Generally robust and complex with a variety of fruit and malt aromas.'
    });
    defaultKegs.push(<V5Types.IKeg>{
      id: 1,
      name: 'Keg2',
      active: false,
      glass: 0,
      pint: 0,
      percentFull: 0,
      stength: 0,
    });
    defaultKegs.push(<V5Types.IKeg>{
      id: 2,
      name: 'Keg3',
      active: false,
      glass: 0,
      pint: 0,
      percentFull: 0,
      stength: 0,
    });
    defaultKegs.push(<V5Types.IKeg>{
      id: 3,
      name: 'Keg4',
      active: false,
      glass: 0,
      pint: 0,
      percentFull: 0,
      stength: 0,
    });
    return defaultKegs;
  }

  // setup default gas levels on device
  public Gas(): V5Types.IGas[] {
    const defaultGas: V5Types.IGas[] = [];
    defaultGas.push(<V5Types.IGas>{
      name: 'Nitrogen',
      status: 'Connected',
      percent: 0,
      percentFull: 0.0,
      colour: '#FFFFFF'
    });
    defaultGas.push(<V5Types.IGas>{
      name: 'CO2',
      status: 'Connected',
      percent: 0,
      percentFull: 0.0,
      colour: '#FFFFFF'
    });
    return defaultGas;
  }

// ------------------------------------------------------------
//   Set the test data for use in testing and on startup
// ------------------------------------------------------------

  public TestOneKeg(): V5Types.IKeg[] {
    const testKegs: V5Types.IKeg[] = [];
      testKegs.push(<V5Types.IKeg>{
      id: 0,
      name: 'T1',
      active: true,
      temp: 1.3,
      glass: 0,
      pint: 0,
      percentFull: 0,
      totalVolume: 50000,  // 50 liters default keg
      servedVolume: 25000,  // half full
      stength: 0,
      beerName: 'test beer',
      description:
        'test one Keg.'
    });
    testKegs.push(<V5Types.IKeg>{
      id: 1,
      name: 'Keg2',
      active: false,
      glass: 0,
      pint: 0,
      percentFull: 0,
      stength: 0,
    });
    testKegs.push(<V5Types.IKeg>{
      id: 2,
      name: 'Keg3',
      active: false,
      glass: 0,
      pint: 0,
      percentFull: 0,
      stength: 0,
    });
    testKegs.push(<V5Types.IKeg>{
      id: 4,
      name: 'Keg4',
      active: false,
      glass: 0,
      pint: 0,
      percentFull: 0,
      stength: 0,
    });
    return testKegs;
  }

  public TestGas(): V5Types.IGas[] {
    const testGas: V5Types.IGas[] = [];
    testGas.push(<V5Types.IGas>{
      name: 'Nitrogen',
      status: 'Connected',
      percent: .5,
      percentFull: 0.5,
      colour: '#FFFFFF'
    });
    testGas.push(<V5Types.IGas>{
      name: 'CO2',
      status: 'Connected',
      percent: .5,
      percentFull: 0.5,
      colour: '#FFFFFF'
    });
    return testGas;
  }
}

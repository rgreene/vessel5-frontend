import { V5Types } from 'src/typings/cordova-typings';
// ---------------------------------
//    All keg calculations needed
// ---------------------------------
export class KegCalculations {
    _keg: V5Types.IKeg;

    PINT_VOLUME = 500;
    GLASS_VOLUME = 200;

    constructor(keg: V5Types.IKeg) {
        this._keg = keg;
    }

    public serve(serveVolumn: number) {
        this._keg.servedVolume = this._keg.servedVolume + serveVolumn;
    }

    public remainingPints(): number {
        return Math.round(this.remainingVolume() / this.PINT_VOLUME);
    }

    public remainingGlasses(): number {
        return Math.round(this.remainingVolume() / this.GLASS_VOLUME);
    }

    public remainingVolume(): number {
        return this._keg.totalVolume - this._keg.servedVolume;
    }

    public remainingPercentage(): number {
        return 1 - (Math.round((this._keg.servedVolume / this._keg.totalVolume) * 100) / 100);
    }
}

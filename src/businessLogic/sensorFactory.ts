import { SerialCommands as COMM } from '../typings/serialCommands';

namespace FactoryMethodPattern {

    export interface AbstractSensor {
        value: number;
        update(param?: any): void;
    }

    export class Tempature implements AbstractSensor {
        value: number;
        update = (param?: any) => {
            this.value = param || 0;
        }
    }

    export class Pressure implements AbstractSensor {
        value: number;
        update = (param?: any) => {
            this.value = param || 0;
        }
    }

    export namespace SensorFactory {

        const sensors = [
            {id: 256, data0: 1, data1: 0, description: 'Condensor Outlet Liquid Temperature', unit: 'c', decimalPlaces: 2, scale: 100, visible: true},
            {id: 257, data0: 1, data1: 1, description: 'Radiator Evaporator Outlet Gas Temperature', unit: 'c', decimalPlaces: 2, scale: 100, visible: true},
            {id: 258, data0: 1, data1: 2, description: 'Plate Evaporator Outlet Gas Temperature', unit: 'c', decimalPlaces: 2, scale: 100, visible: true},
            {id: 259, data0: 1, data1: 3, description: 'Compressor Refrigerant Suction Pressure', unit: 'ph', decimalPlaces: 2, scale: 100, visible: true},
            {id: 260, data0: 1, data1: 4, description: 'Compressor Refrigerant Discharge Pressure', unit: 'ph', decimalPlaces: 2, scale: 100, visible: true},
            {id: 261, data0: 1, data1: 5, description: 'Compressor Case Temperature', unit: 'c', decimalPlaces: 2, scale: 100, visible: true},
            {id: 262, data0: 1, data1: 6, description: 'Compressor Current', unit: 'amp', decimalPlaces: 2, scale: 100, visible: true},
            {id: 263, data0: 1, data1: 7, description: 'Condensor Fan Speed', unit: '', decimalPlaces: 2, scale: 100, visible: true},
            {id: 264, data0: 1, data1: 8, description: 'Radiator Evaporator Calculated Superheat', unit: '', decimalPlaces: 2, scale: 100, visible: true},
            {id: 265, data0: 1, data1: 9, description: 'Plate Evaporator Calculated Superheat', unit: '', decimalPlaces: 2, scale: 100, visible: true},
            {id: 266, data0: 1, data1: 10, description: 'Condensor Calculated Subcooling', unit: '', decimalPlaces: 2, scale: 100, visible: true},
            {id: 267, data0: 1, data1: 11, description: 'Radiator Expansion Valve Position', unit: '', decimalPlaces: 2, scale: 100, visible: true},
            {id: 268, data0: 1, data1: 12, description: 'Plate Expansion Valve Position', unit: '', decimalPlaces: 2, scale: 100, visible: true},
            {id: 512, data0: 2, data1: 0, description: 'Refrigerant Glycol Heat Exchanger Temperature In', unit: 'c', decimalPlaces: 2, scale: 100, visible: true},
            {id: 513, data0: 2, data1: 1, description: 'Refrigerant Glycol Heat Exchanger Temperature Out', unit: 'c', decimalPlaces: 2, scale: 100, visible: true},
            {id: 514, data0: 2, data1: 2, description: 'Glycol Pump Speed', unit: '', decimalPlaces: 2, scale: 100, visible: true},
            {id: 515, data0: 2, data1: 3, description: 'Glycol Flow Volume', unit: '', decimalPlaces: 2, scale: 100, visible: true},
            {id: 516, data0: 2, data1: 4, description: 'Glycol Reservoir Level', unit: 'ml', decimalPlaces: 2, scale: 100, visible: true},
            {id: 517, data0: 2, data1: 5, description: 'Glycol Reservoir Temperature', unit: 'c', decimalPlaces: 2, scale: 100, visible: true},
            {id: 768, data0: 3, data1: 0, description: 'Keg Space Temperature', unit: 'c', decimalPlaces: 2, scale: 100, visible: true},
            {id: 769, data0: 3, data1: 1, description: 'Keg Space Humidity', unit: '', decimalPlaces: 2, scale: 100, visible: true},
            {id: 770, data0: 3, data1: 2, description: 'Keg Space Door Switch', unit: '', decimalPlaces: 2, scale: 100, visible: true},
            {id: 771, data0: 3, data1: 3, description: 'Keg Space Lights', unit: '', decimalPlaces: 2, scale: 100, visible: true},
            {id: 772, data0: 3, data1: 4, description: 'Keg Space Fan Speed', unit: '', decimalPlaces: 2, scale: 100, visible: true},
            {id: 1024, data0: 4, data1: 0,  description: 'Gas Cylinder 1 Installed', unit: '', decimalPlaces: 2, scale: 100, visible: true},
            {id: 1025, data0: 4, data1: 1,  description: 'Gas Cylinder 1 Type', unit: '', decimalPlaces: 2, scale: 100, visible: true},
            {id: 1026, data0: 4, data1: 2,  description: 'Gas Cylinder 1 Weight', unit: 'g', decimalPlaces: 2, scale: 100, visible: true},
            {id: 1280, data0: 5, data1: 0,  description: 'Gas Cylinder 1 Installed', unit: '', decimalPlaces: 2, scale: 100, visible: true},
            {id: 1281, data0: 5, data1: 1,  description: 'Gas Cylinder 1 Type', unit: '', decimalPlaces: 2, scale: 100, visible: true},
            {id: 1282, data0: 5, data1: 2,  description: 'Gas Cylinder 1 Weight', unit: 'g', decimalPlaces: 2, scale: 100, visible: true},
            {id: 2304, data0: 9, data1: 0,  description: 'Heat Exchanger Glycol Temperature In', unit: 'c', decimalPlaces: 2, scale: 100, visible: true},
            {id: 2305, data0: 9, data1: 1,  description: 'Heat Exchanger Glycol Temperature Out', unit: 'c', decimalPlaces: 2, scale: 100, visible: true},
            {id: 2306, data0: 9, data1: 2,  description: 'Glycol Solonoid position', unit: '', decimalPlaces: 2, scale: 100, visible: true},
            {id: 2307, data0: 9, data1: 3,  description: 'Glycol Solonoid Total Energized time', unit: '', decimalPlaces: 2, scale: 100, visible: true},
            {id: 2308, data0: 9, data1: 4,  description: 'Glycol Solonoid Valve operations', unit: '', decimalPlaces: 2, scale: 100, visible: true},
            {id: 2309, data0: 9, data1: 5,  description: 'Glycol Pump Speed', unit: '', decimalPlaces: 2, scale: 100, visible: true},
            {id: 2310, data0: 9, data1: 6,  description: 'Glycol Pump Avg speed', unit: '', decimalPlaces: 2, scale: 100, visible: true},
            {id: 2311, data0: 9, data1: 7,  description: 'Beverage Temperature In', unit: 'c', decimalPlaces: 2, scale: 100, visible: true},
            {id: 2312, data0: 9, data1: 8,  description: 'Beverage Temperature Out', unit: 'c', decimalPlaces: 2, scale: 100, visible: true},
            {id: 2313, data0: 9, data1: 9,  description: 'Beverage Flow Rate', unit: '', decimalPlaces: 2, scale: 100, visible: true},
            {id: 2314, data0: 9, data1: 10,  description: 'Keg Vent Valve', unit: '', decimalPlaces: 2, scale: 100, visible: true},
            {id: 2315, data0: 9, data1: 11,  description: 'Keg Pressure', unit: 'ph', decimalPlaces: 2, scale: 100, visible: true},
            {id: 2316, data0: 9, data1: 12,  description: 'Increase C02 Pressure Valve', unit: '', decimalPlaces: 2, scale: 100, visible: true},
            {id: 2317, data0: 9, data1: 13,  description: 'Total Volume Served', unit: 'ml', decimalPlaces: 2, scale: 100, visible: true},
            {id: 2318, data0: 9, data1: 14,  description: 'Number of Beverages Served', unit: '', decimalPlaces: 2, scale: 100, visible: true},
            {id: 2319, data0: 9, data1: 15,  description: 'Volume Of Last served', unit: 'ml', decimalPlaces: 2, scale: 100, visible: true},
            {id: 2320, data0: 9, data1: 16,  description: 'Average Flow Rate', unit: '', decimalPlaces: 2, scale: 100, visible: true},
            {id: 2321, data0: 9, data1: 17,  description: 'Flow Rate Of Last Rate', unit: '', decimalPlaces: 2, scale: 100, visible: true},
            {id: 2323, data0: 9, data1: 19,  description: 'Average serve tempature', unit: 'c', decimalPlaces: 2, scale: 100, visible: true}
        ];

        export function createSensor(command: COMM.ISerialComm): AbstractSensor {
            // sensor is based on the Data0 and Data1 fields in the format
            const sensor = sensors.filter((s) => s.data0 === command.Data0);
            if (sensor) {
                return new Tempature();
            }
            return null;
        }
    }
}

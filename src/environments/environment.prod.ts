export const environment = {
  production: true,
  SERIAL_DRIVER: 'CdcAcmSerialDriver',
  SERIAL_SPEED: 57600,
  SERIAL_DELIMITOR: '\n',
  API_USERAUTH_ENDPOINT: 'http://vessel5api-dev.ap-southeast-2.elasticbeanstalk.com/api/Account/Login',
  API_DEVICE_ENDPOINT: 'http://vessel5api-dev.ap-southeast-2.elasticbeanstalk.com/api/Device',
  API_KEY: 'w3D4COe0gf6obfYPTnpVz1mKzPi9PvB92qbTfC93',
  DEVICE_ID: 1,
  SecureStorageKey: 'Vessel5SecureStorageKey',
  LOCAL_DB_NAME: `Vessel5Local`
};

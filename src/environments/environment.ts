import 'zone.js/dist/zone-error'; // Included with Angular CLI.

// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  SERIAL_DRIVER: 'CdcAcmSerialDriver',
  SERIAL_SPEED: 57600,
  SERIAL_DELIMITOR: '\n',
  API_USERAUTH_ENDPOINT: '/api/Account/Login',
  API_DEVICE_ENDPOINT: '/api/Prod',
  API_KEY: 'w3D4COe0gf6obfYPTnpVz1mKzPi9PvB92qbTfC93',
  DEVICE_ID: 1,
  SecureStorageKey: 'Vessel5SecureStorageKey',
  LOCAL_DB_NAME: `Vessel5Local`
};

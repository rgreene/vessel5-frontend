import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainPage } from './pages/main/main.page';
import { StartPage } from './pages/start/start.page';

const routes: Routes = [
  { path: '', redirectTo: 'start', pathMatch: 'full' },
  { path: 'main', component: MainPage },
  { path: 'connection', loadChildren: './pages/connection/connection.module#ConnectionPageModule' },
  { path: 'device', loadChildren: './pages/device/device.module#DevicePageModule' },
  { path: 'error', loadChildren: './pages/error/error.module#ErrorPageModule' },
  { path: 'language-location', loadChildren: './pages/language-location/language-location.module#LanguageLocationPageModule' },
  { path: 'product/:id', loadChildren: './pages/product/product.module#ProductPageModule' },
  { path: 'report', loadChildren: './pages/report/report.module#ReportPageModule' },
  { path: 'settings', loadChildren: './pages/settings/settings.module#SettingsPageModule' },
  { path: 'start', component: StartPage },
  { path: 'store', loadChildren: './pages/store/store.module#StorePageModule' },
  { path: 'welcome', loadChildren: './pages/welcome/welcome.module#WelcomePageModule' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

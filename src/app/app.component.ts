import { Component } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { Router} from '@angular/router';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {

  pages: Array<{title: string, icon: string, route: string}>;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private menu: MenuController,
    private readonly router: Router,
  ) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Dashboard', icon: 'th', route: 'main' },
 //     { title: 'Purchase', icon: 'shopping-cart', route: 'purchase' },
      { title: 'Reports', icon: 'bar-chart', route: 'report' },
      { title: 'Language', icon: 'globe', route: 'language-location' },
      { title: 'Connection', icon: 'wifi', route: 'connection' },
      { title: 'User Settings', icon: 'user', route: 'settings' },
      { title: 'Device', icon: 'cog', route: 'device'},
      { title: 'Start', icon: 'cogs', route: 'start' },
  //    { title: 'Welcome', icon:  'wrench', route: 'welcome'},
  //    { title: 'Error', icon: 'exclamation', route: 'error' },
    ];
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      setTimeout(() => {
        this.splashScreen.hide();
      }, 4000);
    });
  }

  public openPage(page: any) {
    if (!page) {return; }
    this.router.navigate([page.route]);
    this.menu.close();
  }
}

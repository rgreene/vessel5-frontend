import { TestBed } from '@angular/core/testing';

import { ServerAPIService } from './server-api.service';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { Serial, SerialOpenOptions } from '@ionic-native/serial/ngx';
import { Device } from '@ionic-native/device/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';

describe('ServerAPIService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [ BrowserModule, HttpModule ],
    providers: [Serial, Device, Geolocation],
  }));

  it('should be created', () => {
    const service: ServerAPIService = TestBed.get(ServerAPIService);
    expect(service).toBeTruthy();
  });
});

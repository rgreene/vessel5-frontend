import { TestBed } from '@angular/core/testing';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { Serial, SerialOpenOptions } from '@ionic-native/serial/ngx';
import { Device } from '@ionic-native/device/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { SerialConnectionService } from './serial-connection.service';

describe('SerialConnectionService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [ BrowserModule, HttpModule ],
    providers: [Serial, Device, Geolocation],
  }));
  
  it('should be created', () => {
    const service: SerialConnectionService = TestBed.get(SerialConnectionService);
    expect(service).toBeTruthy();
  });
});

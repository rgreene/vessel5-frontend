import { Injectable } from '@angular/core';
import { DatabaseRecords as DBR } from '../../typings/databaseRecords';
import { Subject } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoggingService {
  // Log entries
  logEntries: DBR.ILogEntry[] = [];
  private logEntry = new Subject<DBR.ILogEntry>();
  logEntry$ = this.logEntry.asObservable();
  private dbName = 'vesselDB.db';
  private dbTableCreate = `CREATE TABLE IF NOT EXISTS logging
                           (id INTEGER PRIMARY KEY, eventType TEXT, date DATETIME, message TEXT, value TEXT)`;
  private db: any;

  constructor() {
  }

  // log information
  info(entry: string) {
    this.addEntry(<DBR.ILogEntry>{ eventType : 'INFO', message : entry });
    if (!environment.production) { console.log(entry); }
  }

  // log warning
  warning(entry: string) {
    this.addEntry(<DBR.ILogEntry>{ eventType : 'WARNING', message : entry });
    if (!environment.production) { console.warn(entry); }
  }

  // log error
  error(entry: string) {
    this.addEntry(<DBR.ILogEntry>{ eventType : 'ERROR', message : entry });
    if (!environment.production) { console.error(entry); }
  }

  // ---
  // -- Read the records from the database
  // ---
  // async getLogs(recordLimit : number = 1000) : Promise<DBR.ILogEntry[]>  {
  //   // don't log if the DB is unavailable
  //   if(!this.db) return;
  //   // create a new entry
  //   await this.db.executeSql('SELECT * FROM logging ORDER BY id DESC LIMIT ?', [recordLimit] )
  //       .then((res) => {
  //         this.logEntries = [];
  //         if (res.rows.length > 0) {
  //           for (var i = 0; i < res.rows.length; i++) {
  //             var event : DBR.ILogEntry = {
  //               id : res.rows.item(i).id,
  //               eventType : res.rows.item(i).eventType,
  //               date : res.rows.item(i).date,
  //               message : res.rows.item(i).message,
  //               value : res.rows.item(i).value,
  //             }
  //             this.logEntries.push(event);
  //           }
  //         }
  //         return this.logEntries;
  //       })
  //       .catch(e => { this.logEntries.push(
  //         <DBR.ILogEntry> { id:0, eventType :'DB_ERROR', message : "insrt failed", value : e });
  //         console.log("error logging");
  //         console.log(e);
  //       });
  // }

  // ---
  // -- Add item to the database
  // ---
  private async addEntry(entry: DBR.ILogEntry) {
    // publish the log
    console.log(`Log: ${entry.message}`);
    this.logEntry.next(entry);
    // don't log if the DB is unavailable

    // if(!this.db) return;
    // // create a new entry
    // await this.db.executeSql('INSERT INTO logging VALUES (NULL, ?, CURRENT_TIMESTAMP, ?, ?)',
    //                     [entry.eventType, entry.message, entry.value])
    //                     .catch(e => {
    //                       console.log("error inserting log");
    //                       this.logEntries.push(
    //                       <DBR.ILogEntry> { id:0, eventType :'DB_ERROR', message : "insert failed", value : e })
    //                     });
  }

  // ---
  // -- Init the database and the logging table
  // ---
  public async startLogging() {
  //   await this.sqlite.create({
  //     name: this.dbName,
  //     location: 'default'
  //   }).then(async (db: SQLiteObject) => {
  //     await db.executeSql(this.dbTableCreate, {})
  //     .then(res => {
  //       this.db = db;
  //       return;
  //     })
  //     .catch(e => console.log(e));
  // })
}

// -- clear the logging table
public clearDB() {
  // if(!this.db) return;
  // this.db.executeSql('DELETE FROM logging', {})
  //   .then(res => {
  //     this.logEntries = [];
  //   })
  //   .catch(e => console.log(e));
}

// -- delete the local database
public deleteDB() {
  //   this.sqlite.deleteDatabase({
  //   name: this.dbName,
  //   location: 'default'
  // }).then((db: SQLiteObject) => {
  //     this.db = null;
  //   })
  //   .catch(e => console.log(e));
  }
}

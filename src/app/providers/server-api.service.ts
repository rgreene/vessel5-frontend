import { Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { Device } from '@ionic-native/device/ngx';
import { LoggingService } from './logging.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { SerialCommands as COMM } from '../../typings/serialCommands';
import { APIContracts as API } from '../../typings/APIContracts';
import { Platform } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ServerAPIService {
  latitude: Number;
  longitude: number;

  constructor(
    private readonly http: Http,
    private readonly device: Device,
    private readonly geolocation: Geolocation,
    private readonly logging: LoggingService,
    private readonly platform: Platform
  ) {

    // exit if we are on the browser
    this.platform.ready().then((env) => {
      this.logging.info(env);
      if (env === 'cordova') {
        // get current gps just once as we're not very pushed on accurate details
        this.geolocation
          .getCurrentPosition({
            enableHighAccuracy: true,
            maximumAge: 300000,
            timeout: 5000,
          })
          .then((position: any) => {
            this.longitude = position.coords.longitude;
            this.latitude = position.coords.latitude;
            this.logging.info(`Location. ${this.latitude} / ${this.longitude}`);
          })
          .catch((err: any) => {
            this.logging.error(`Error getting location. ${err.code} / ${err.message}`);
          });
      }
    });
  }

  // standard Header for API call
  headers: Headers = new Headers({
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'POST',
    'Accept': 'application/json',
    'x-api-key': environment.API_KEY
  });

  // Send a message to the cloud
  public async sendDeviceMessage(message: COMM.ISerialComm) {
    // send the message
    const data = this.GenerateDeviceMessage(message);
    // fire and forget
    this.http
      .post(`${environment.API_DEVICE_ENDPOINT}`, data, { headers: this.headers })
      .subscribe(
        () => { /* do nohting */},
        err => this.logging.error('data not sent to server : ' + err)
      );
  }

  // Format the message for sending to the cloud
  private GenerateDeviceMessage(message: COMM.ISerialComm): API.IDeviceEvent {
    return <API.IDeviceEvent>{
      UUID: this.device.uuid || '000',
      DeviceID: environment.DEVICE_ID || 0,

      // -- Values
      ProtocolVersion: message.ProtocolVersion,
      SenderModuleType: message.SenderModuleType,
      SenderModuleID: message.SenderModuleID,
      TargetModuleType: message.TargetModuleType,
      TargetModuleID: message.TargetModuleID,
      Instruction: message.Instruction,
      DataSize: message.DataSize,
      Data0: message.Data0,
      Data1: message.Data1,
      Data2: message.Data2,
      Data3: message.Data3,
      Data4: message.Data4,
      Data5: message.Data5,
      Data6: message.Data6,
      Data7: message.Data7,

      // -- Location
      Lng: this.longitude || 0,
      Lat: this.latitude || 0,
      // -- vector sequence
      sequence: 1,
      Message: 'Device Event'
    };
  }
}

import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Serial, SerialOpenOptions } from '@ionic-native/serial/ngx';
import { SerialCommands as COMM } from '../../typings/serialCommands';
import { Platform } from '@ionic/angular';
import { ServerAPIService } from './server-api.service';
import { LoggingService } from './logging.service';
import { Subject, BehaviorSubject } from 'rxjs';
import { environment } from '../../environments/environment';
import { SecureStorageService } from './secure-storage.service';
@Injectable({
  providedIn: 'root'
})
export class SerialConnectionService {

  /*
  allows for reading and writing to the various connected devices
  */
  public driver: string;
  public baud: number;

  // Observable string streams
  public devicePortOpen = new BehaviorSubject<boolean>(false);
  devicePortOpen$ = this.devicePortOpen.asObservable();
  public deviceConnected = new BehaviorSubject<boolean>(false);
  deviceConnected$ = this.deviceConnected.asObservable();
  public prevCommand = new Subject<COMM.ISerialComm>();
  prevCommand$ = this.prevCommand.asObservable();

  // command in progress
  public currentCommand = '';
  public errorString: string;

  constructor(
    public http: Http,
    private readonly serial: Serial,
    private readonly api: ServerAPIService,
    private readonly secureStorage: SecureStorageService,
    private readonly platform: Platform,
    private readonly logging: LoggingService) {

    // exit if we are on the browser
    this.platform.ready().then((env) => {
      logging.info('SerialConnectionService started');
      if (env === 'cordova') {
        // register command creator for any text arriving
        this.serial.registerReadCallback().subscribe((data: any) => {
          this.buildCommand(this.ab2str(data));
        });
      }
      if (env !== 'cordova') {
        /// tsting so fake serial connected
        this.deviceConnected.next(true);
      }
    });
  }

  public async request(driver: string = null): Promise<boolean> {
    // request permission
    return await this.requestPermission(driver || environment.SERIAL_DRIVER)
    .then((permission: boolean) => {
      this.logging.info(`permission given.. ${permission}`);
      this.deviceConnected.next(permission);
      return permission;
    })
    .catch((err) => {
      this.logging.warning('Error getting permisson:' + err);
      this.deviceConnected.next(false);
      return false;
    });
  }

  public async start(baud: number) {
    // Open the connection
    await this.open(baud)
    .then((res) => {
      this.devicePortOpen.next(true);
      this.deviceConnected.next(true);
      return true;
    })
    .catch((err) => {
      this.devicePortOpen.next(false);
      this.logging.error('Error opening port');
    });
  }

  public async stop() {
    // request permission
    await this.closeSerialDataConnection()
    .then((res) => {
      this.devicePortOpen.next(false);
      this.deviceConnected.next(false);
    })
    .catch((err) => {
      this.deviceConnected.next(false);
      this.logging.error('Error closing port');
    });
  }

  public async open(baud: number): Promise<any> {
    await this.openSerialDataConnection(baud)
      .then((data) => {
        this.logging.info('opened..');
        this.deviceConnected.next(true);
        return true;
      })
      .catch((err) => {
        this.logging.error(err);
        return false;
      });
  }

  // Send a command via the provider to the serial connection
  // delimitator is atomatically added to the command
  // Command are send to the device using the format
  // SYS TX: 0 2 1 1 1 0 8 128 0 0 0 0 0 0 0 \n
  //
  public async writeData(data: COMM.ISerialComm ) {
    const command = `SYS TX: ${data.ProtocolVersion} ${data.SenderModuleType} ${data.SenderModuleID} ${data.TargetModuleType} ${data.TargetModuleID} ${data.Instruction} ${data.DataSize} ${data.Data0} ${data.Data1} ${data.Data2} ${data.Data3} ${data.Data4} ${data.Data5} ${data.Data6} ${data.Data7}`;
    this.serial.write(command + environment.SERIAL_DELIMITOR)
      .then((res) => {})
      .catch((error: any) => this.logging.error(error));
  }

  public async stopData() {
    this.logging.info('Stopping data');
    await this.closeSerialDataConnection();
  }

  //  Request permission
  private async requestPermission(driver: string): Promise<boolean> {
    const opt = { vid: '04D8', pid: '00DF', driver: driver };
    return await this.serial.requestPermission(opt)
      .then(() => {
        this.logging.info('permission given!!!');
        return true;
      })
      .catch(() => {
        this.logging.warning('permission not given.');
        this.deviceConnected.next(false);
        return false;
      });
  }

  // Open the connection
  private async openSerialDataConnection(baud: number): Promise<any> {
    try {
      const options: SerialOpenOptions = { baudRate: baud,
        dataBits : 8,
        stopBits: 1,
        parity: 0,
        dtr: false,
        rts: false,
        sleepOnPause: false };
      await this.serial.open(options)
        .then(() => {
          this.logging.info('opened');
          return (true);
        })
        .catch((error: any) => {
          this.logging.error('error ' + error);
          return (false);
        });
    } catch (err) {
      this.logging.error('error:' + err);
      return (false);
    }
  }

  //  Close the connection
  private async closeSerialDataConnection(): Promise<any> {
    try {
      await this.serial.close(
      )
        .then(() => {
          this.logging.info('closed serial connection');
          return (true);
        })
        .catch((error: any) => {
          this.logging.error('error closing connection' + error);
          return (false);
        });
    } catch (err) {
      this.logging.warning('unexpected error closing connection:' + err);
      return (false);
    }
  }

  //  Build a command from the fragments provided by the serial connection
  // check if the delimitator has been passed if so execute the command
  // Command are send to the device using the format
  // TX DATA: 0 2 1 1 1 0 8 128 0 0 0 0 0 0 0 \n
  // Commands are returned from device using format
  // RX DATA: 0 1 0 2 1 1 8 128 0 0 D1 D2 D3 D4 0 \n"
  //
  private buildCommand(text: string) {
    const terminator = text.indexOf(environment.SERIAL_DELIMITOR);
    // have we reached the command terminator?
    if (terminator === -1) {
      this.currentCommand = this.currentCommand.concat(text);
      return;
    }
    console.log(this.currentCommand);

    this.currentCommand = this.currentCommand.concat(text.substring(0, terminator));
    /* convert the string into a valid object
    // example
    // RX DATA: 0 1 1 2 1 1 8 128 1 1 27 0 0 0 0
    // only send the SYS RX messages for the moment
    */
    if (this.currentCommand.startsWith('RX DATA:')) {
      // any envent such as serving a bring should be persisted
      this.secureStorage.persistState(this.toSerialCommand(this.currentCommand));
      this.api.sendDeviceMessage(this.toSerialCommand(this.currentCommand));
      this.prevCommand.next(this.toSerialCommand(this.currentCommand));
    }
    // publish the command to any subscribers
    this.currentCommand = text.substring(terminator + 1);
  }

  /*  Simple function to test serial connections
  */
  public sendTestMessage(testCommand: string) {
    this.secureStorage.persistState(this.toSerialCommand(testCommand));
    this.prevCommand.next(this.toSerialCommand(testCommand));
  }

  // take a standard string and parse it for the various details
  private toSerialCommand(command: string): COMM.ISerialComm {
    // remove the header
    command = command.replace('RX DATA: ', '');
      const params: string[] = command.split(' ');
      const result = <COMM.ISerialComm> {
        ProtocolVersion : Number(params[0]),
        SenderModuleType : Number(params[1]),
        SenderModuleID : Number(params[2]),
        TargetModuleType : Number(params[3]),
        TargetModuleID : Number(params[4]),
        Instruction : Number(params[5]),
        DataSize : Number(params[6]),
        Data0 : Number(params[7]),
        Data1 : Number(params[8]),
        Data2 : Number(params[9]),
        Data3	: Number(params[10]),
        Data4	: Number(params[11]),
        Data5	: Number(params[12]),
        Data6	: Number(params[13]),
        Data7 : Number(params[14])
      };
      return result;
  }

  // simple tranform process
  private ab2str(buf): string {
    return String.fromCharCode.apply(null, new Uint8Array(buf));
  }
}

import { Platform } from '@ionic/angular';
import { LoggingService } from './logging.service';
import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { Storage } from '@ionic/storage';
import { environment } from 'src/environments/environment';
import { V5Types } from './../../typings/cordova-typings';
import { Subject } from 'rxjs';
import { SerialCommands as COMM } from '../../typings/serialCommands';
import { KegCalculations } from 'src/businessLogic/kegCaclulations';

@Injectable()
/// Set any local items from secure storage
export class SecureStorageService {

  public _userPreference: V5Types.IUserPreference;
  public _kegStatus: V5Types.IKeg[];
  public _gasStatus: V5Types.IGas[];

  public UserPreference: any = new Subject<V5Types.IUserPreference>();
  public kegStatus: any = new Subject<V5Types.IKeg[]>();
  public gasStatus: any = new Subject<V5Types.IGas[]>();

  UserPreference$ = this.UserPreference.asObservable();
  kegStatus$ = this.kegStatus.asObservable();
  gasStatus$ = this.gasStatus.asObservable();

  readonly USER_PREFERENCES = 'preferencesKey';
  readonly KEG_STATUS = 'kegsKey';
  readonly GAS_STATUS = 'gasKey';

  constructor(
    private readonly logger: LoggingService,
    private readonly platform: Platform,
    private readonly storage: Storage,
    private readonly sqlite: SQLite
    ) {
      // create the local DB if needed
      this.platform.ready().then((env) => {
        logger.info('storage started');
        if (env === 'cordova') {
          this.createLocalDB();
        }
      });
  }

  // --------------------------------------
  //   Persist Event as it effects state
  // --------------------------------------
  public async persistState(data: COMM.ISerialComm) {
    // Messages that effect state are:
    // -- Serve
    // -- Gas
    // -- Configuration
    // Serve has taken place DATA7 holds the keg ID
    if (data.Data0 === 9 && data.Data1 === 15) {
        if (!this._kegStatus || !this._kegStatus[data.Data7]) {
          this.logger.warning(`-- no kegs in local storage status --`);
          return;
        }
        const keg = this._kegStatus[data.Data7];
        const calc = new KegCalculations(keg);
        const _serveVolumn = Math.round(((data.Data3 * 16777216) + (data.Data4 * 65536) + (data.Data5 * 256) + data.Data6 || 0)) / 100;
        calc.serve(_serveVolumn);
        keg.pint = calc.remainingPints();
        keg.glass = calc.remainingGlasses();
        keg.percentFull = calc.remainingPercentage();
        // set the new values and record the new state
        this._kegStatus[data.Data7] = keg;
        this.save().then(() => {
          this.recordServe(<V5Types.IServeData>{ id: keg.id, amount: _serveVolumn });
        });
      }
    }

  public async refresh() {
    // load all the values needed
    this.logger.info(`refreshing storage`);
    this._userPreference = await this.load(this.USER_PREFERENCES);
    this._kegStatus = await this.load(this.KEG_STATUS);
    this._gasStatus = await this.load(this.GAS_STATUS);

    this.gasStatus.next(this._gasStatus);
    this.UserPreference.next(this._userPreference);
    this.kegStatus.next(this._kegStatus);
  }

  // -----------------------
  //      set the features
  // ------------------------
  public async setUser(user: V5Types.IUserPreference) {
    this._userPreference = user;
  }
  public async setKegs(kegs: V5Types.IKeg[]) {
    this._kegStatus = kegs;
  }
  // simple set the value of the keg passed
  public async setKeg(keg: V5Types.IKeg) {
    this.logger.info(`setting Keg value ${keg.id}`);
    this._kegStatus[keg.id] = keg;
  }
  public async setGas(gas: V5Types.IGas[]) {
    this._gasStatus = gas;
  }

  // -----------------------
  //      serve Details
  // ------------------------

  public async recordServe(serve: V5Types.IServeData) {
    // get the serves for this keg in teh last 7 days
      this.sqlite.create({
        name: environment.LOCAL_DB_NAME,
        location: 'default'
      })
      .then((db: SQLiteObject) => {
        db.executeSql(`INSERT INT serves ( keg_id, date, amount) VALUES (?) `, [serve])
        .then((success) => this.logger.info(`serve recorded from keg ${serve.id}.`))
        .catch(e => this.logger.error(`Error getting serve data: ${e}`));
      });
  }

  public async getServesByHour(id: number): Promise<any> {
    // get the serves for this keg in the last 24 hours
    console.log(`here + ${environment.LOCAL_DB_NAME}`);
    return new Promise((resolve, reject) => {
      this.sqlite.create({
        name: environment.LOCAL_DB_NAME,
        location: 'default'
      })
        .then((db: SQLiteObject) => {
          console.log('here1');
          db.executeSql(`SELECT * FROM serves WHERE keg_id=${id} ORDER BY rowid DESC`)
          .then(res => {
            console.log(`res`);
            console.log(res);
            resolve(res.rows.map((data: any) => <V5Types.IServeData>{ id: data.keg_id, date: data.date, amount: data.amount } ));
          })
          .catch(e => {
            this.logger.error(`Error getting serve data: ${e}`);
            reject(`error getting serve data`);
          });
        })
        .catch(e => {
          this.logger.error(`Error getting serve data: ${e}`);
          reject(`error getting serve data`);
        });
    });
  }

  public async getServesByDay(id: number): Promise<any> {
    // get the serves for this keg in teh last 7 days
    return new Promise((resolve, reject) => {
      this.sqlite.create({
        name: environment.LOCAL_DB_NAME,
        location: 'default'
      })
        .then((db: SQLiteObject) => {
          db.executeSql(`SELECT * FROM serves WHERE keg_id=${id} ORDER BY rowid DESC`)
          .then(res => {
            console.log(`res`);
            console.log(res);
            resolve(res.rows.map((data: any) => <V5Types.IServeData>{ id: data.keg_id, date: data.date, amount: data.amount } ));
          })
          .catch(e => {
            this.logger.error(`Error getting serve data: ${e}`);
            reject(`error getting serve data`);
          });
        })
        .catch(e => {
          this.logger.error(`Error getting serve data: ${e}`);
          reject(`error getting serve data`);
        });
    });
  }


  // -----------------------
  //      Private functions
  // ------------------------
  private async load(key: string): Promise<any> {
   // load users preferences
   const data = await this.storage.get(key);
    if (data) {
      return data;
    }
    return null;
  }

  public async save() {
    this.logger.info(`save`);
    // save user details
    await this.storage.set(this.USER_PREFERENCES, this._userPreference);
    // save keg details
    await this.storage.set(this.KEG_STATUS, this._kegStatus);
    // save gas details
    await this.storage.set(this.GAS_STATUS, this._gasStatus);
  }

  public async clear() {
    this.logger.info(`clear storage`);
    // save user details
    await this.delete(this.USER_PREFERENCES);
    await this.delete(this.KEG_STATUS);
    await this.delete(this.GAS_STATUS);
  }

  private async delete(key: string) {
    this.storage.remove(key)
    .then((result) =>  this.logger.info(`delete result ${result}`) )
    .catch((err) => { this.logger.error(`error on delete ${err}`); });
  }

  // ---------------------------
  //      Local DB Functions
  // ---------------------------
  private async createLocalDB() {
    this.logger.info('createLocalDB..');
    this.sqlite.create({
      name: environment.LOCAL_DB_NAME,
      location: 'default'
    })
      .then((db: SQLiteObject) => {
        db.executeSql(`CREATE TABLE IF NOT EXISTS serves(id INTEGER PRIMARY KEY, keg_id INT, date Text DEFAULT DATETIME('now'), amount int)`, [])
          .then(() => this.logger.info('Table created if needed.'))
          .catch(e => this.logger.info(`Error creating table: ${e}`));
      })
      .catch(e => console.log(e));
    }
}

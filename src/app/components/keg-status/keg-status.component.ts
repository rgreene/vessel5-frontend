import { LoggingService } from './../../providers/logging.service';
import { Component, OnInit, OnDestroy, Input, Output, EventEmitter, NgZone } from '@angular/core';
import { V5Types } from '../../../typings/cordova-typings';
import { KegCalculations } from 'src/businessLogic/kegCaclulations';
import { SerialConnectionService } from 'src/app/providers/serial-connection.service';
import { SerialCommands as COMM } from '../../../typings/serialCommands';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-keg-status',
  templateUrl: './keg-status.component.html',
  styleUrls: ['./keg-status.component.scss']
})
export class KegStatusComponent implements OnInit, OnDestroy {
  private _serialSubscription: Subscription;

  public _kegClassName = 'active';
  public _keg: V5Types.IKeg;
  public _logo = 'icon-glass-beer.svg';
  public _totalVolume: number;
  public _servedVolume: number;

  @Input()
  set keg(keg: V5Types.IKeg) {
    const calc = new KegCalculations(keg);
    this._keg = keg;
    this._keg.pint = calc.remainingPints();
    this._keg.glass = calc.remainingGlasses();
    this._keg.percentFull = calc.remainingPercentage();
    this._kegClassName = this._keg.active ? 'active' : 'inactive';
  }
  @Output()
  selected: EventEmitter<V5Types.IKeg> = new EventEmitter<V5Types.IKeg>();

  constructor(
    private readonly logger: LoggingService,
    private readonly serial: SerialConnectionService,
    private readonly zone: NgZone
  ) {

    }

    ngOnInit() {

     // check on any value coming back on serial
     this._serialSubscription = this.serial.prevCommand$.subscribe((data: COMM.ISerialComm) => {
      if (data.Data7 !== this._keg.id) {
         return;
      }

      this.logger.info(`message for this keg ${data.Data7} / ${this._keg.id}`);
      // if this is a temp sensor update the average temp
      if (data.Data0 === 9 && data.Data1 === 8) {
        this.zone.run(() => {
          const value = (((data.Data3 * 16777216) + (data.Data4 * 65536) + (data.Data5 * 256) + data.Data6 || 0));
          this._keg.temp = Math.round((( value / 100) * 10 ) / 10);
          // test if negative value first bit is 1
          if (data.Data3.toString(2).charAt(0) === '1') {
            this._keg.temp = Math.round(((4294967296 - value) / 100) * 10) / 10;
          }
        });
      }

      // serve takes place
      if (data.Data0 === 9 && data.Data1 === 15) {
        this.zone.run(() => {
          this.logger.info(`update serve`);
          const calc = new KegCalculations(this._keg);
          const _serveVolumn = Math.round(((data.Data3 * 16777216) + (data.Data4 * 65536) + (data.Data5 * 256) + data.Data6 || 0)) / 100;
          this.logger.info(`${_serveVolumn}`);
          calc.serve(_serveVolumn);
          this._keg.pint = calc.remainingPints();
          this._keg.glass = calc.remainingGlasses();
          this._keg.percentFull = calc.remainingPercentage();
        });
      }
    });
  }

    ngOnDestroy() {
      this._serialSubscription.unsubscribe();
    }

    select() {
      this.selected.emit(this._keg);
    }
}

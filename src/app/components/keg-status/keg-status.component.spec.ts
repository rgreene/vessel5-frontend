import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KegStatusComponent } from './keg-status.component';

describe('KegStatusComponent', () => {
  let component: KegStatusComponent;
  let fixture: ComponentFixture<KegStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KegStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KegStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BluetoothSetupComponent } from './bluetooth-setup.component';

describe('BluetoothSetupComponent', () => {
  let component: BluetoothSetupComponent;
  let fixture: ComponentFixture<BluetoothSetupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BluetoothSetupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BluetoothSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

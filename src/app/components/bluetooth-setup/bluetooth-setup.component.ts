import { Component, OnInit } from '@angular/core';
import { V5Types } from 'src/typings/cordova-typings';

@Component({
  selector: 'app-bluetooth-setup',
  templateUrl: './bluetooth-setup.component.html',
  styleUrls: ['./bluetooth-setup.component.scss']
})
export class BluetoothSetupComponent implements OnInit {

  public network: V5Types.IBluetoothNetwork[] = [];

  constructor() { }

  ngOnInit() {
    this.network.push(<V5Types.IBluetoothNetwork>{name: 'Vessel 5', icon: 'bluetooth'});
  }

}

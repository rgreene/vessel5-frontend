import { CommonModule } from '@angular/common';
import { BluetoothSetupComponent } from './bluetooth-setup/bluetooth-setup.component';
import { NetworkSetupComponent } from './network-setup/network-setup.component';
import { LanguageComponent } from './language/language.component';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { UnderConstructionComponent } from './under-construction/under-construction.component';
import { LocationComponent } from './location/location.component';
import { GasStatusComponent } from './gas-status/gas-status.component';
import { ChartComponent } from './chart/chart.component';
import { KegStatusComponent } from '../components/keg-status/keg-status.component';
import { SelectKegComponent } from '../components/select-keg/select-keg.component';
import { StartPage } from '../pages/start/start.page';
import { MainPage } from '../pages/main/main.page';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    UnderConstructionComponent,
    KegStatusComponent,
    GasStatusComponent,
    ChartComponent,
    LanguageComponent,
    LocationComponent,
    NetworkSetupComponent,
    BluetoothSetupComponent,
    SelectKegComponent,
    StartPage,
    MainPage
  ],
  exports: [
    UnderConstructionComponent,
    KegStatusComponent,
    GasStatusComponent,
    ChartComponent,
    LocationComponent,
    LanguageComponent,
    NetworkSetupComponent,
    BluetoothSetupComponent,
    SelectKegComponent,
    StartPage,
    MainPage
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
}) export class SharedModule {

}

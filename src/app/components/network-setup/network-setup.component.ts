import { Component, OnInit } from '@angular/core';
import { V5Types } from 'src/typings/cordova-typings';

@Component({
  selector: 'app-network-setup',
  templateUrl: './network-setup.component.html',
  styleUrls: ['./network-setup.component.scss']
})
export class NetworkSetupComponent implements OnInit {
  public network: V5Types.IWifiNetwork[] = [];

  constructor() {}

  ngOnInit() {
    this.network.push(<V5Types.IWifiNetwork>{ name: 'Wi-fi1', icon: 'wifi' });
    this.network.push(<V5Types.IWifiNetwork>{ name: 'Wi-fi2', icon: 'wifi' });
    this.network.push(<V5Types.IWifiNetwork>{ name: 'Wi-fi3', icon: 'wifi' });
    this.network.push(<V5Types.IWifiNetwork>{ name: 'Wi-fi4', icon: 'wifi' });
  }
}

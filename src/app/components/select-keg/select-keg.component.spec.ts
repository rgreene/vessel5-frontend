import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectKegComponent } from './select-keg.component';

describe('SelectKegComponent', () => {
  let component: SelectKegComponent;
  let fixture: ComponentFixture<SelectKegComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectKegComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectKegComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

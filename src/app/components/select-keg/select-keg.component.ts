import { LoggingService } from './../../providers/logging.service';
import { V5Types } from 'src/typings/cordova-typings';
import { Component, OnInit, Input } from '@angular/core';
import { SecureStorageService } from 'src/app/providers/secure-storage.service';
import { NavController } from '@ionic/angular';
import { DefaultObjects } from 'src/businessLogic/defaultDataObjects';
import { logging } from 'protractor';

@Component({
  selector: 'app-select-keg',
  templateUrl: './select-keg.component.html',
  styleUrls: ['./select-keg.component.scss']
})
export class SelectKegComponent implements OnInit {

  @Input()
  set changingKeg(changingKeg: boolean) {
    console.log('changing!' + changingKeg);
    this._changingKeg = changingKeg;
  }
  @Input()
  set selectedKeg(keg: V5Types.IKeg) {
    this._keg = keg;
  }

  _keg: V5Types.IKeg;
  _beverages: Array<V5Types.IBeverage> = [];
  _changingKeg = false;

  constructor(
    private readonly logging: LoggingService,
    private readonly secureStorage: SecureStorageService,
    private readonly navCtrl: NavController,
  ) {
  }

  ngOnInit() {
    // load all the kegs
    this.loadKegCatalogue();
  }

  async selectKeg(event, item: V5Types.IBeverage) {
    this.logging.info(`Keg has been changed to ${item.name}`);
    // reset the storage to reflect a new Keg
    const newKeg: V5Types.IKeg = new DefaultObjects().Default50LiterKeg();
    this.logging.info('new');
    this.logging.info(JSON.stringify(newKeg));
    this.logging.info('_keg');
    this.logging.info(JSON.stringify(this._keg));
    newKeg.id = this._keg.id;
    newKeg.beerName = item.name;
    newKeg.description = item.description;
    newKeg.logo = item.logo;
    this.secureStorage._kegStatus[this._keg.id] = newKeg;
    this.secureStorage.save().then(async () => {
      // return to the dashboard
      await this.navCtrl.navigateRoot(`/main`);
    });
  }

  async loadKegCatalogue() {
   this._beverages.push( <V5Types.IBeverage>{
    id: 1,
    name: 'Little Creatures'
   });
   this._beverages.push( <V5Types.IBeverage>{
    id: 2,
    name: 'Single Fin'
    });
    this._beverages.push( <V5Types.IBeverage>{
    id: 3,
    name: 'Some other beer'
    });
    this._beverages.push( <V5Types.IBeverage>{
    id: 4,
    name: 'Coopers'
  });
  }
}

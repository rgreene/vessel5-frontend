import { Chart } from 'chart.js';
import { Component, OnInit, ViewChild, Input } from '@angular/core';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit {
  _canvas: Chart;
  _graphType: string;
  _graphData: any;
  _graphOptions: any;

  @Input()
  set graphType(graphType: string) {
    this._graphType = graphType || 'bar';
  }

  @Input()
  set graphData(graphData: any) {
    console.log(graphData);
    this._graphData = graphData || null;
    this.createGraph();
  }

  @Input()
  set graphOptions(graphOptions: any) {
    console.log(graphOptions);
    this._graphOptions = graphOptions || {
                                          responsive: true,
                                          maintainAspectRatio: false,
                                          scales: {
                                            yAxes: [
                                              {
                                                ticks: {
                                                  beginAtZero: true
                                                }
                                              }
                                            ]
                                          }
                                        };
  }

  @ViewChild('graphCanvas') graphCanvas;
  constructor() {}

  public ngOnInit(): void {}

  private createGraph() {
    this._canvas = new Chart(this.graphCanvas.nativeElement, {
      type: this._graphType,
      data: this._graphData,
      options: this._graphOptions
    });
  }

  public update() {
     this._canvas.update();
  }
}

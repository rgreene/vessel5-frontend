import { Component, OnInit } from '@angular/core';
import { V5Types } from 'src/typings/cordova-typings';

@Component({
  selector: 'app-language',
  templateUrl: './language.component.html',
  styleUrls: ['./language.component.scss']
})
export class LanguageComponent implements OnInit {
  public languages: V5Types.ILanguage[] = [];

  constructor() {
    this.languages.push(<V5Types.ILanguage>{ name: 'English', icon: 'flag' });
    this.languages.push(<V5Types.ILanguage>{ name: 'French', icon: 'flag' });
    this.languages.push(<V5Types.ILanguage>{ name: 'German', icon: 'flag' });
    this.languages.push(<V5Types.ILanguage>{ name: 'Italian', icon: 'flag' });
  }

  ngOnInit() {}
}

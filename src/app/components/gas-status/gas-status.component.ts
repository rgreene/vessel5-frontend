import { Component, OnInit, Input } from '@angular/core';
import { V5Types } from '../../../typings/cordova-typings';

@Component({
  selector: 'app-gas-status',
  templateUrl: './gas-status.component.html',
  styleUrls: ['./gas-status.component.scss']
})
export class GasStatusComponent implements OnInit {

  _gas: V5Types.IGas;

  @Input()
  set gas(gas: V5Types.IGas) {
    this._gas = gas;
  }

  constructor() {
    this._gas = <V5Types.IGas>{
      colour : '',
      status : '',
      percentFull : 50,
      percent : .5,
      name : 'unknown'
    };
  }

  ngOnInit() {
  }

}




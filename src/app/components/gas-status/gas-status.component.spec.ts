import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GasStatusComponent } from './gas-status.component';

describe('GasStatusComponent', () => {
  let component: GasStatusComponent;
  let fixture: ComponentFixture<GasStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GasStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GasStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

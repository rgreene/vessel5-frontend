import { ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SerialConnectionService } from './providers/serial-connection.service';
import { LoggingService } from './providers/logging.service';
import { SecureStorageService } from './providers/secure-storage.service';

@NgModule({
  imports:      [ CommonModule ],
  providers:    [
    SecureStorageService,
    SerialConnectionService,
    LoggingService
  ]
})
export class CoreModule {
  constructor (@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error(
        'CoreModule is already loaded. Import it in the AppModule only');
    }
  }

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: [
        SecureStorageService,
        SerialConnectionService,
        LoggingService]
    };
  }
}
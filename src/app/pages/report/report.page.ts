import { Component, OnInit, ViewChild, AfterContentInit } from '@angular/core';
import { ChartComponent } from '../../components/chart/chart.component';

@Component({
  selector: 'app-report',
  templateUrl: './report.page.html',
  styleUrls: ['./report.page.scss'],
})
export class ReportPage implements OnInit, AfterContentInit {

  _barData: any;
  _graphOption: any[];
  _graphPeriod: any[];

  @ViewChild('chart1') chart1: ChartComponent;

  constructor() { }

  ngOnInit() {

  }

  ngAfterContentInit() {
    console.log('cccc');
    this._graphOption = [
      { id: 1, title: 'Running Costs', selected: true},
      { id: 2, title: 'Income', selected: false },
      { id: 3, title: 'Beverage', selected: false}];

      this._graphPeriod = [
        { id: 1, title: 'Weekly', selected: true,
          graphData: {
            labels: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
            datasets: [{
                label: '# of Votes',
                data: [12, 19, 3, 5, 2, 3, 7, 5],
                borderWidth: 1
            }]}
        },
        { id: 2, title: 'Monthly', selected: false,
          graphData: {
            labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'],
            datasets: [{
                label: '# ',
                data: [12, 19, 3, 5, 2, 3, 7, 12, 19, 3, 5, 2],
                borderWidth: 1
            }]}
        },
        { id: 3, title: 'Quarterly', selected: false,
          graphData: {
            labels: ['Q1', 'Q2', 'Q3', 'Q4'],
            datasets: [{
                label: '#',
                data: [40, 20, 30, 70],
                borderWidth: 1
            }]}
      },
      { id: 4, title: 'Custom', selected: false,
        graphData: {
          labels: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
          datasets: [{
              label: '#',
              data: [12, 19, 3, 5, 2, 3, 7],
              borderWidth: 1
          }]}
      }];

    this.updateGraph();
  }

  public setGraphOption(event: Event) {
    // clear selection
    this._graphOption.forEach(x => x.selected = false);
    // set the selected
    const elementId: string = (event.target as Element).id;
    const option = this._graphOption.find(x => x.id === elementId);
    option.selected = true;
    const index = this._graphOption.indexOf(option);
    this._graphOption[index] = option;
    this.updateGraph();
  }

  public setGraphPeriod(event: Event) {
    // clear selection
    this._graphPeriod.forEach(x => x.selected = false);
    // set the selected
    const elementId: string = (event.target as Element).id;
    const option = this._graphPeriod.find(x => x.id === elementId);
    option.selected = true;
    const index = this._graphPeriod.indexOf(option);
    this._graphPeriod[index] = option;
    this.updateGraph();
  }

  private updateGraph() {
    const option = this._graphPeriod.find(x => x.selected);
    console.log(option);
    this._barData = option.graphData;
    this.chart1.update();
  }
}


import { LoggingService } from './../../providers/logging.service';
import { Component, OnInit, Inject } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { V5Types } from 'src/typings/cordova-typings';
import { Platform, NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { SecureStorageService } from 'src/app/providers/secure-storage.service';
import * as moment from 'moment';

@Component({
  selector: 'app-product',
  templateUrl: './product.page.html',
  styleUrls: ['./product.page.scss']
})
export class ProductPage implements OnInit {
  _keg: V5Types.IKeg;
  _kegs: V5Types.IKeg[];
  _hourData: any;
  _dayData: any;
  _dayChart = true;
  _changingKeg = false;
  _graphOptions = {
    responsive: false,
    maintainAspectRatio: false,
    height: 500,
    scales: {
      xAxes: [
        {
          type: 'time',
          distribution: 'series',
          ticks: {
            source: 'data',
            autoSkip: true
          }
        }
      ],
      yAxes: [
        {
          scaleLabel: {
            display: true,
            labelString: 'Amount served (ml)'
          }
        }
      ]
    }
  };

  constructor(
    private readonly navCtrl: NavController,
    private readonly activatedRoute: ActivatedRoute,
    private readonly secureStroage: SecureStorageService,
    private readonly logging: LoggingService,
    private readonly alertCtrl: AlertController
  ) {
    // hourly dataset for the last 24 hours
    this._hourData = {
      datasets: [
        {
          data: this.timeBasedOnRange(24, 'hours').map(date => ({
            t: date.valueOf(),
            y: 0
          }))
        }
      ]
    };
    this._dayData = {
      datasets: [
        {
          data: this.timeBasedOnRange(7, 'days').map(date => ({
            t: date.valueOf(),
            y: 3
          }))
        }
      ]
    };

    // get the kegs from the database
    const id = Number(this.activatedRoute.snapshot.paramMap.get('id'));
    const kegs = this.secureStroage._kegStatus.filter(k => k.id === id);
    if (kegs) {
      this._keg = kegs[0];
    }
  }

  async ngOnInit() {
    // load the serve data if keg is selected

    if (!this._keg) {
      return;
    }
    console.log(`getting ${this._keg.id}`);
    this._hourData = await this.secureStroage.getServesByHour(this._keg.id);
    //    this._dayData = await this.secureStroage.getServesByDay(this._keg.id);
    //    this.logging.info(JSON.stringify(this._hourData));
  }

  async back() {
    await this.navCtrl.navigateRoot(`/main`);
  }

  async toggleChart() {
    this._dayChart = !this._dayChart;
  }

  private timeBasedOnRange(iterations: number, step: string): any[] {
    console.log('ger graph');

    const result = [];
    let time;
    for (let i: any = 0; i < iterations; i++) {
      // fill in all of the values
      time = moment().subtract(i, step); // give the time in format X AM/PM
      result.unshift(time); // add to beginning of array
    }
    return result;
  }

  async changeKegRequest() {
    const alert = await this.alertCtrl.create({
      header: 'Confirm!',
      message: 'Are you sure you wish to change the Keg?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            this.logging.info('Confirm of change Keg canceled');
            this._changingKeg = false;
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this._changingKeg = true;
            this.logging.info('Confirm of change Keg recieved');
          }
        }
      ]
    });
    const result = await alert.present();
    this.logging.info(`result:`);
    this.logging.info(JSON.stringify(result));
  }
}

import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LanguageLocationPage } from './language-location.page';

describe('LanguageLocationPage', () => {
  let component: LanguageLocationPage;
  let fixture: ComponentFixture<LanguageLocationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LanguageLocationPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LanguageLocationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { TestBed } from '@angular/core/testing';
import { V5Types } from './../../../typings/cordova-typings';

import { Component, OnInit, OnDestroy } from '@angular/core';
import { NavController, Platform } from '@ionic/angular';
import { SerialConnectionService } from '../../providers/serial-connection.service';
import { LoggingService } from './../../providers/logging.service';
import { SecureStorageService } from './../../providers/secure-storage.service';
import { environment } from 'src/environments/environment.prod';
import { DefaultObjects } from 'src/businessLogic/defaultDataObjects';

@Component({
  selector: 'app-start',
  templateUrl: './start.page.html',
  styleUrls: ['./start.page.scss']
})
export class StartPage implements OnInit, OnDestroy {
  private _default = new DefaultObjects();  // default data

  constructor(
    private serial: SerialConnectionService,
    public readonly secureStroageService: SecureStorageService,
    public readonly logging: LoggingService,
    private readonly navCtrl: NavController,
    private readonly platform: Platform
  ) {

    this.logging.info('Start Page start');

    this.secureStroageService.UserPreference$.subscribe(async (data: V5Types.IUserPreference) => {
      if (data != null) {
        this.navCtrl.navigateRoot('/main')
        .then((result) =>  this.logging.info(`result ${result}`) )
        .catch((err) => { this.logging.error(`error on navigation ${err}`); });
        return;
      }
      this.logging.info(`reset to default`);
      // -- default
      this.secureStroageService.setUser(this._default.User());
      this.secureStroageService.setKegs(this._default.Kegs());
      this.secureStroageService.setGas(this._default.Gas());
      this.secureStroageService.save()
      .then(() => {
        this.secureStroageService.refresh();
      });
    });
  }

  ngOnInit() {
    this.logging.info('Startup loaded');
    // check to see if we have existing data stored

    this.platform.ready().then(async () => {
      if (this.platform.is('android') === true) {
       this.startSerial();
       this.secureStroageService.refresh();
      }
      /// -- when running in the desktop use these settings
      if (this.platform.is('desktop') === true) {
        // setup for default Testing
        this.secureStroageService.setUser(this._default.User());
        this.secureStroageService.setKegs(this._default.TestOneKeg());
        this.secureStroageService.setGas(this._default.TestGas());
        // persist the results and refresh
        await this.secureStroageService.save()
        .then(() => this.secureStroageService.refresh());
       }
    });
  }

  ngOnDestroy() {
    this.logging.info('exit page');
  }

  public async startSerial() {
    // start serial connection
    this.logging.info('serial starting on Start Page');
    await this.serial.request(environment.SERIAL_DRIVER)
    .then(async (result) => {
      if (!result) { return; }
      await this.serial.start(environment.SERIAL_SPEED)
      .then(() => this.logging.info('serial started'))
      .catch(() => this.logging.error('error starting serial'));
    })
    .catch(() => this.logging.error('error requesting serial'));
  }

}



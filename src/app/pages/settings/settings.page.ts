import { V5Types } from 'src/typings/cordova-typings';
import { LoggingService } from './../../providers/logging.service';
import { Component, OnInit } from '@angular/core';
import { SecureStorageService } from 'src/app/providers/secure-storage.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Http, Headers } from '@angular/http';
import { environment } from '../../../environments/environment';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {

  userForm: FormGroup;

  // standard Header for API call
  headers: Headers = new Headers({
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'POST',
    'Accept': 'application/json',
    'x-api-key': environment.API_KEY
  });

  constructor(
    private readonly logger: LoggingService,
    private readonly secureStore: SecureStorageService,
    private formBuilder: FormBuilder,
    private readonly toastCtrl: ToastController,
    private readonly http: Http) {
      // load the existing prefenrece

      this.userForm = this.formBuilder.group({
        email: ['', Validators.compose([Validators.maxLength(30), Validators.email, Validators.required])], // Setting fields as required
        password: ['', Validators.compose([Validators.maxLength(30), Validators.required])]
      });

    }

  ngOnInit() {

  }

  async login() {
    if (this.userForm.valid) {
      // error message for being unable to login
      const data = this.userForm.value;
      this.logger.info(JSON.stringify(this.headers));
      this.http
      .post(`${environment.API_USERAUTH_ENDPOINT}`, data, { headers: this.headers })
      .subscribe(
        async () => {
          this.logger.error('User has logged in to server ');
          await this.successLogging();
        },
        async err => {
          this.logger.error('User was unable to login to server : ' + err);
          await this.errorLogging();
        }
      );
    }
  }

  async errorLogging() {
    const toast = await this.toastCtrl.create({
      message: 'Unable to login, please try again.',
      duration: 3000
    });
    toast.present();
  }

  async successLogging() {
    const toast = await this.toastCtrl.create({
      message: 'Login Successful',
      duration: 3000
    });
    toast.present();
    // store the login details in the secure store
    const userPref: V5Types.IUserAccount = {
      fullName: this.userForm.value.email,
      email: this.userForm.value.email
    };
  }

}

import { Platform, ToastController, NavController } from '@ionic/angular';
import { ServerAPIService } from './../../providers/server-api.service';
import { Component, OnInit } from '@angular/core';
import { SerialConnectionService } from '../../providers/serial-connection.service';
import { LoggingService } from '../../providers/logging.service';
import { SerialCommands as COMM} from '../../../typings/serialCommands';
import { DatabaseRecords as DB} from '../../../typings/databaseRecords';
import { SecureStorageService } from 'src/app/providers/secure-storage.service';

@Component({
  selector: 'app-device',
  templateUrl: './device.page.html',
  styleUrls: ['./device.page.scss'],
})
export class DevicePage implements OnInit {

  logMessage = '';
  portStatus: boolean;
  portData: COMM.ISerialComm;
  version = '1.0.0';
  deviceIP = '127.0.0.1';
  deviceId = 'xxxxx-xxxxxxx-xxxxxxx-xxxxx';
  cloudVersion = '1.0.1';

  constructor(
    private readonly serial: SerialConnectionService,
    private readonly api: ServerAPIService,
    private readonly logging: LoggingService,
    private readonly secureStorage: SecureStorageService,
    private readonly toastCtrl: ToastController,
    private readonly platform: Platform,
    private readonly navCtrl: NavController
    ) {
  }

  ngOnInit() {
  }

  ionViewDidLoad() {
    this.logging.info('ionViewDidLoad DevicePage');

    // get the status
    this.serial.deviceConnected$.subscribe((data) => {
      this.logging.info('device event fired');
      this.portStatus = data;
    });
    // get the last command
    this.serial.prevCommand$.subscribe((data) => {
      this.portData = data;
    });
    this.logging.logEntry$.subscribe((data) => {
      this.logMessage = JSON.stringify(data);
    });

  }

  // Start the serial listener
  public async startSerial(driver: string) {
      // start serial connection
      console.log('startSerial..');
      await this.serial.request()
      .then(async (result) => {
        if (!result) { return; }
        await this.serial.start(57600)
        .then(() => this.logging.info('serial started'))
        .catch(() => this.logging.error('error starting serial'));
      })
      .catch(() => this.logging.error('error requesting serial'));
  }

  ///  Do a factory reset
  async factoryReset() {
    // clear out the storage
    const toast = await this.toastCtrl.create({
      message: 'device reset.',
      duration: 2000
    });
    await this.secureStorage.clear()
    .then(async () => {
      await this.secureStorage.refresh();
      // send a message
      toast.present();
      this.navCtrl.navigateRoot('/start');
    });
  }

  public sendServeMessage() {
    this.logging.info(`send test serve`);
    this.serial.sendTestMessage(`RX DATA: 0 1 1 2 1 1 8 9 15 1 0 0 165 165 0`);
  }

  // Start the serial listener
  public async sendMessage() {
      // start serial connection
      console.log('sendMessage..');
      const message: COMM.ISerialComm = {
        ProtocolVersion : 5,
        SenderModuleType : 5,
        SenderModuleID : 6,
        TargetModuleType : 6,
        TargetModuleID : 6,
        Instruction : 7,
        DataSize : 8,
        Data0 : 9,
        Data1 : 9,
        Data2 : 9,
        Data3	: 9,
        Data4	: 9,
        Data5	: 9,
        Data6	: 9,
        Data7 : 9
      };
      await this.api.sendDeviceMessage(message);
    }
}

import { SerialConnectionService } from './../../providers/serial-connection.service';
import { Component, OnInit, ChangeDetectorRef, NgZone } from '@angular/core';
import { V5Types } from '../../../typings/cordova-typings';
import { Platform, NavController, ToastController } from '@ionic/angular';
import { LoggingService } from './../../providers/logging.service';
import { SecureStorageService } from 'src/app/providers/secure-storage.service';
import { SerialCommands as COMM } from '../../../typings/serialCommands';
import { DefaultObjects } from 'src/businessLogic/defaultDataObjects';
import { KegCalculations } from 'src/businessLogic/kegCaclulations';

@Component({
  selector: 'app-main',
  templateUrl: './main.page.html',
  styleUrls: ['./main.page.scss']
})
export class MainPage implements OnInit {
  public Kegs: V5Types.IKeg[] = [];
  public Gases: V5Types.IGas[] = [];
  public _keg: V5Types.IKeg;
  public serialEnabled = true;

  constructor(
    public readonly logger: LoggingService,
    public readonly secureStroageService: SecureStorageService,
    private readonly platform: Platform,
    private readonly navCtrl: NavController,
    private readonly toastCtrl: ToastController,
    private serial: SerialConnectionService,
  ) {
    this.logger.info('main Page start');
    // keg update
    this.secureStroageService.kegStatus$.subscribe((data: V5Types.IKeg[]) => {
      this.Kegs = data;
    });

    // gas update
    this.secureStroageService.gasStatus$.subscribe((data: V5Types.IGas[]) => {
      this.Gases = data;
    });

    // enable or diable the app if serial is connected
    this.serial.deviceConnected$.subscribe((data: boolean) => {
      this.logger.info(`return from serial status:${data}`);
      this.serialEnabled = data;
    });

  }

  ngOnInit() {
    this.platform.ready().then(() => {
      this.secureStroageService.refresh();
      this.logger.info(`init from page :${this.serial.deviceConnected.getValue()}`);
      this.serialEnabled = this.serial.deviceConnected.getValue();
    });
  }

  // go to the product page based on the selected keg
  async selectKeg(keg: V5Types.IKeg) {
    const param: any = { keg: keg };
    if (keg.active) {
      await this.navCtrl.navigateRoot(`/product/${keg.id}`);
    } else {
      const toast = await this.toastCtrl.create({
        message: 'No details available for this keg.',
        duration: 2000
      });
      toast.present();
    }
  }

  public async startSerial() {
    // start serial connection
    this.logger.info('serial starting on Start Page');
    await this.serial.request()
    .then(async (result) => {
      if (!result) { return; }
      await this.serial.start(57600)
      .then(() => this.logger.info('serial started'))
      .catch(() => this.logger.error('error starting serial'));
    })
    .catch(() => this.logger.error('error requesting serial'));
}

  public fakePermission() {
    this.serial.deviceConnected.next(true);
  }

}

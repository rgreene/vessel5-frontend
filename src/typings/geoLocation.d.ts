export namespace GeoLocation {

    export interface IGeoLocation
    {
    city : string;
    lat : number;
    lng : number;
    }
}
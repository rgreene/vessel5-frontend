/*  
  Database records

  Variables use const whereas properties use readonly.
  
  readonly 
  Suggestion:
  1. DevicePreferences 
  2. LogSystem for the device operating 
  4. RecipeBeer 

*/
export namespace DatabaseRecords {

  export interface DeviceSetting {
    _id: string, 
    _rev?: string, 
    timestamp?: Date,
    language: string, 
    location: Location,
    user: V5User,
    workWeek: WorkWeek,
    selectedBeverage?: SelectedBeverage,
    selectedRecipeID: string
  }

  export interface Location {
    City: string, 
    latitude?: number,
    longitude?: number    
  }

  // depreched, will be removed once everything is ok
  export interface V5preference {
    // a interface struct for all the setting for the fridge..

    _id: string,                    // Fridge ID
    type: string,                   // preference
    language: string,               // Used prefered language
    location?: string,              // Location of the Device
    beerSizes?: BeerSizes,          // Specify what beer sizes exist.... ?? 
  }



  export interface V5User {
    _id: string,                  // user id e.g. user1, user2
    type: string,                 // userData
    firstName: string,
    lastName: string,
    userName: string,
    lastLoggedIn: string,
    lastLoggedOut: string,
    timeZoneOffset: string,
    email: string,
    passcode: string
  }

  // TODO: Should we store the Working hours this way? or in date...
  export interface WorkWeek {
    _id: string,
    type: string,
    days: [WorkDay,WorkDay,WorkDay,WorkDay,WorkDay,WorkDay]  // 7 Days fixed
    
  }

  interface WorkDay {
    day: string, 
    start: number; //between 0-23
    finish: number; // between 0-23
  }

  // Do we need this.???? or simplify this structure.
  export interface SelectedBeverage {
    _id: string,      //currentBeverage
    name?: string,
    brand: string,
    kegLevel?: number,
    kegSize: number,
    co2Level?: number,
    co2Size?: number,
    kwh?: number
  }

  // TODO: need specification
  interface KegSize { }
  interface CO2Size { }

  // Should Define, what size is a pint, scooner, jug etc.    for Analytics..
  export interface BeerSizes {
    _ìd: string, // location
    Jug: number,
    Pint: number,
    Scooner: number,
    size4: number,
    // ...    
  }
  export interface RecipeBeer {
    _id: string,          // name of Beer recipe
    _rev?: string,
    brand: string,
    beerType?: string, 
    brewrieName: string, 
    brewrieDescription?: string,
    brewrieDetails?: string,
    brewrieWebpage?: string,   
    serveTemp: number,    // in centiCelsius
    kegTemp: number,      // Target kegspace temp in centiCelsius
    gasName: string,        // default CO2 
    gasTempcc: [number, number, number, number, number],  // 0, 10 20 30,40 (last is max)
    gasPressure: [number, number, number, number, number] // 

    serves?: number,      // if fixed serving sizes this is >0  
    _attachments?: any,    // for Picture... - Image for Beer and Image for brwries
  
  }

   // --- Recipe stuff..
   export interface RecipeBeer2 {
    _id: string,          // name of Beer recipe
    _rev?: string,
    brand: string,
    beerType?: string, 
    brewrie?: brewries,
    serveTemp: number,    // in centiCelsius
    kegTemp: number,      // Target kegspace temp in centiCelsius
    gas: GasType,         // default CO2, change to array, when more gastypes are available.
    serves?: number,      // if fixed serving sizes this is >0  
    _attachments?: any,    // for Picture... - Image for Beer and Image for brwries
  }

  interface GasType {
    // keg temp ca. 12...
    GasName: string,        // default CO2 
    tempcc: [number, number, number, number, number],  // 0, 10 20 30,40 (last is max)
    pressure: [number, number, number, number, number] // 
  }

  interface brewries {
    name: string, 
    description?: string,
    brewriesDetails?: string,
    webpage?: string,   
  }

  // ----- Serial  ----------


  // ---- local Logging 
  export interface ILogEntry {
    id: number,        
    date: string,
    eventType: string,
    message?: string,
    value?: string,
  }

}


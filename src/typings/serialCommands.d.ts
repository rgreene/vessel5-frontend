/*  
  Communcation protocol with master serial device
  instructions are derived from the information provided 
  in the Protocol excel spreadsheet.  See Brendan for details
*/
export namespace SerialCommands {

    // standard Send/Receive message 
    export interface ISerialComm {
      ProtocolVersion : number;
      SenderModuleType : SenderModuleType;
      SenderModuleID : number;
      TargetModuleType : TargetModuleType;
      TargetModuleID : number;
      Instruction : number;
      DataSize : number;
      Data0 : number;
      Data1 : number;
      Data2 : number;
      Data3	: number;
      Data4	: number;
      Data5	: number;
      Data6	: number;
      Data7 : number;
    }
  
    // -- Expect module list to grow
    export enum SenderModuleType{
        MainController = 1,
        CloudDevice,
        TemperatureSensor,
        InputModule,
        OutputModule,
        HVModule,
        LoadSensorModule
    }
  
    // -- export types of modules to grow
    export enum TargetModuleType {
        Global = 0,
        MainController,
        CloudDevice,
        TemperatureSensor,
        InputModule,
        OutputModule,
        HVModule,
        LoadSensorModule
    }
  
    // read a value / write a value
    export enum Instruction {
      RequestReadParameter = 0,
      RequestReadParameterResponse,
      RequestWriteParameter,
      RequestWriteParameterResponse
    }
  
  }
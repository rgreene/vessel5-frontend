export namespace V5Types {
  export interface IKeg {
    id: number;
    name: string;
    beerName: string;
    description: string;
    active: boolean;
    temp: number;
    glass: number;
    servedVolume: number;
    totalVolume: number;
    percentFull: number;
    pint: number;
    stength: number;
    logo: string;
  }

  export interface IGas {
    colour: string;
    status: string;
    percentFull: number;
    percent: number;
    name: string;
  }

  export interface ILanguage {
    name: string;
    icon: string;
  }
  export interface IWifiNetwork {
    name: string;
    icon: string;
  }
  export interface IBluetoothNetwork {
    name: string;
    icon: string;
  }

  export interface IUserPreference {
    setup: boolean;
    deviceName: string;
    deviceUUID: string;
    deviceLocation: string;
    user: IUserAccount;
  }

  export interface IUserAccount {
    fullName: string;
    email: string;
  }

  export interface IBeverage {
    id: number, 
    name: string,
    logo?: string,    
    brewery?: string,    
    description?: string    
  }

  export interface IServeData {
    id: number, 
    date: string,
    amount?: number    
  }

}

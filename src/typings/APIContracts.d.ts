
// API Communication Contracts
export namespace APIContracts {

    export interface IDeviceEvent {
        UUID : string;
        DeviceID : number;

        // -- Values
        ProtocolVersion : number;
        SenderModuleType : number;
        SenderModuleID : number;
        TargetModuleType : number;
        TargetModuleID: number;
        Instruction: number;
        DataSize : number;
        Data0 : number;
        Data1 : number;
        Data2 : number;
        Data3 : number;
        Data4 : number;
        Data5 : number;
        Data6 : number;
        Data7 : number;

        // -- Location
        Lng : number;
        Lat : number;
        // -- vector sequence
        sequence: number;
        Message : string;
    }

    export interface apiVer {
        copywrite: string;
        status: string;
        version: string;
        setupComplete: string;
    }

    export interface IUser {
        name: string,
        email: string,
        dob: string,
        username: string,
        passcode: string
    }
}